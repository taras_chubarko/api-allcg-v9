sudo docker-compose up -d nginx mongo redis workspace
sudo docker-compose build workspace php-fpm
sudo docker-compose exec workspace bash
sudo docker-compose down
php artisan db:seed --class=NotificationSeeder
php artisan ide-helper:generate
php artisan test --filter 'Tests\\Feature\\TransactionsTipsNotifyTest'
$users = $this->users()->with('roles')->get();
return $users ? $users->where('roles.slug', 'owner')->first() : null;
chown -R 1000:1000 /var/www

ide-helper:eloquent  Add \Eloquent helper to \Eloquent\Model
ide-helper:generate  Generate a new IDE Helper file.
ide-helper:meta      Generate metadata for PhpStorm
ide-helper:models    Generate autocompletion for models
