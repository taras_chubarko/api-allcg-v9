<?php

namespace Database\Factories;

use App\Models\ProductTariff;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {

        $price = $this->faker->randomElement(ProductTariff::query()->get());
        $user = $this->faker->randomElement(User::all());
        $formats = ['js', 'svg', 'png', 'map', 'css', 'less', 'ttf', 'eot', 'woff', 'woff2', 'otf', 'jpg', 'txt', 'html', 'scss'];

        $previews = [];
        $folder = Str::random(10);
        Storage::makeDirectory('product/' . $folder);

        foreach (range(0, 3) as $item) {
            $name = Str::random(10) . '.png';
            $img = \Image::make(storage_path('app/product/default/') . $this->faker->numberBetween(1, 10). '.jpeg');
            $img->save(storage_path('app/product/' . $folder . '/' . $name), 100, 'png');

            $previews[] = [
                'name' => $name,
                'path' => 'product/' . $folder . '/' . $name,
                'size' => 80946,
                'type' => 'image/png'
            ];
        }

        return [
            'name' => $this->faker->sentence(3),
            'description' => $this->faker->text(500),
            'category' => ['5b06d906bd30255c722ae2e1', '5b06d906bd30255c722ae2f3', '5b06d906bd30255c722ae2f4'],
            'tags' => ['Лазур', 'Програмне забезпечення', 'Схил', 'Технологія'],
            'settings' => [
                'component' => 'FieldsWebHTML',
                'fields' => [
                    'columns' => '5e85faa6d1669c600a48a526',
                    'compatible_browsers' => '5e85f25fd1669c599110ad4b',
                    'compatible_with' => '5e85f474d1669c76725b1e17',
                    'demo_url' => null,
                    'files_included' => '5e85f9a3d1669c599a2226be',
                    'high_resolution' => '5caf38d7bd302550067b32d1',
                    'layout' => '5e85fb34d1669c598f784327',
                ]
            ],
            'price' => [
                'id' => $price->id,
                'amount' => $price->world['amount'],
                'amount_sng' => $price->sng['amount'],
                'category' => $price->category,
            ],
            'files' => [
                'archives' => [
                    [
                        'name' => 'simplemaps_worldcities_basicv1.74.zip',
                        'path' => 'archives/default/simplemaps_worldcities_basicv1.74.zip',
                        'size' => 4192705,
                        'type' => 'application/zip',
                        'formats' => $this->faker->randomElements($formats, 3)
                    ]
                ],
                'previews' => $previews
            ],
            'reklama' => [],
            'user_id' => $user->id,
            'status' => 1,
        ];
    }

}
