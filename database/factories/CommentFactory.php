<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comment>
 */
class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $created =  $this->faker->dateTimeInInterval('-2 weeks');
        $products = Product::query()->select('_id')->get()->pluck('_id')->toArray();
        $users = User::query()->select('_id')->get()->pluck('_id')->toArray();

        return [
            'entity' => 'product',
            'entity_id' => $this->faker->randomElement($products),
            'comment' => $this->faker->text(200),
            'user_id' => $this->faker->randomElement($users),
            'updated_at' => $created,
            'created_at' => $created,
        ];
    }
}
