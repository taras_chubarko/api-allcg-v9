<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductView>
 */
class ProductViewFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $created =  $this->faker->dateTimeInInterval('-2 weeks');
        $products = Product::query()->select('_id')->get()->pluck('_id')->toArray();
        return [
            'product_id' => $this->faker->randomElement($products),
            'ip' => $this->faker->ipv4,
            'updated_at' => $created,
            'created_at' => $created,
        ];
    }
}
