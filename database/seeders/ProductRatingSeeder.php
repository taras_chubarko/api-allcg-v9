<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductRatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();
        foreach ($products as $product){
            $product->rate = $product->reviews->count() ? round($product->reviews->pluck('rating')->sum() / $product->reviews->count()) : 0;
            $product->save();
        }
    }
}
