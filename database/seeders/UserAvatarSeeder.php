<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserAvatarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::query()->get();
        foreach ($users as $user) {
            $name = $user->id . '.png';
            $folder = $user->id;
            \Storage::makeDirectory('public/avatar/' . $folder);
            \Avatar::create($user->full_name)->save(storage_path('app/public/avatar/' . $folder . '/' . $name));
            $user->avatar = 'avatar/' . $folder . '/' . $name;
            $user->save();
        }
    }
}
