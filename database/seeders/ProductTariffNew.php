<?php

namespace Database\Seeders;

use App\Models\ProductTariff;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductTariffNew extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tariffs = config('tariff');
        ProductTariff::query()->delete();
        foreach ($tariffs as $tariff){
            ProductTariff::query()->create($tariff);
        }
    }
}
