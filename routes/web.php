<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
//    $fondy = new \App\Classes\Fondy();
//    $data = $fondy->create([
//        'order_id' => 'all-cg-1110002',
//        'amount' => 10,
//        'sender_email' => 'tchubarko@gmail.com',
//    ]);
    //dd(Hash::make('10.22.15.32'));
    //$data = \App\Models\Order::query()->find('62443b42e7003d171a03c627');
   // $data->update(['status' => 1]);
    //dd(\Carbon\Carbon::now());
    //$data->save();
    //dd($data);
    //$data = \Illuminate\Support\Facades\Redis::set('qq', '111');

});

Route::post('fondy/callback', [\App\Http\Controllers\FondyController::class, 'complete']);

Route::get('/download/{crypt}', function ($crypt, \App\Services\Contracts\ProductServiceInterface $productService) {
    return response()->download($productService->download($crypt));
});

require __DIR__.'/downloads.php';

require __DIR__.'/images.php';
