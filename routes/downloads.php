<?php

use App\Http\Middleware\AuthMiddleware;
use App\Models\User\Profile\Chat\ChatMessageAttachment;
use Illuminate\Support\Facades\Route;
use App\Models\User\Profile\Chat\Chat;

Route::group(['prefix' => 'download'], function () {
    Route::get('/chat-attachments/{attachment_id}', function ($attachment_id) {
        $attachment = ChatMessageAttachment::findOrFail($attachment_id);
        $path = storage_path('app/' . $attachment->path);
        return response()->download($path, $attachment->name);
    })->name('download.chat_attachments');
});


