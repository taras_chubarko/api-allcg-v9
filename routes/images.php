<?php

use App\Models\User\Profile\Chat\ChatMessageAttachment;
use Illuminate\Support\Facades\Route;

Route::get('/chat_attachments/images/{id}/{filename}', function ($id) {
    $chatMessageAttachment = ChatMessageAttachment::find($id);

    if (!$chatMessageAttachment || !str_contains($chatMessageAttachment->type, 'image')) {
        abort(404);
    }

    return response()->file(storage_path('app/' . $chatMessageAttachment->path));
})->name('chat_attachments.images');



