<?php

namespace App\Observers;

use App\Facades\UserFacade;
use App\Models\User;

class UserObserver
{
    //
    public function created(User $user)
    {
        if($user->is_activate === false){
            UserFacade::setDefaultFields($user);
        }
    }
}
