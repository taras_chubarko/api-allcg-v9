<?php

namespace App\Observers;

use App\Models\UserPrivate;

class UserPrivateObserver
{
    public function creating(UserPrivate $userPrivate)
    {
        $userPrivate->scan_inn_verify_at = null;
        $userPrivate->scan_passport_at = null;
    }

    public function updating(UserPrivate $userPrivate)
    {
        if($userPrivate->getOriginal('scan_inn') !== $userPrivate->scan_inn){
            $userPrivate->scan_inn_verify_at = null;
        }
        if($userPrivate->getOriginal('scan_passport') !== $userPrivate->scan_passport){
            $userPrivate->scan_passport_at = null;
        }
    }
}
