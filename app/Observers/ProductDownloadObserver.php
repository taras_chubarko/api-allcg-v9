<?php

namespace App\Observers;

use App\Facades\ProductFacade;
use App\Models\ProductDownload;

class ProductDownloadObserver
{
    //
    public function created(ProductDownload $productDownload)
    {
        ProductFacade::calcDownload($productDownload);
    }
}
