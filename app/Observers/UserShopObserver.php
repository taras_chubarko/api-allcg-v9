<?php

namespace App\Observers;

use App\Models\UserShop;

class UserShopObserver
{
    public function creating(UserShop $userShop)
    {
        $userShop->scan_inn_verify_at = null;
        $userShop->scan_passport_at = null;
    }

    public function updating(UserShop $userShop)
    {
        if($userShop->getOriginal('scan_inn') !== $userShop->scan_inn){
            $userShop->scan_inn_verify_at = null;
        }
        if($userShop->getOriginal('scan_passport') !== $userShop->scan_passport){
            $userShop->scan_passport_at = null;
        }
    }
}
