<?php

namespace App\Observers;

use App\Models\UserBuyedTariff;

class UserBuyedTariffObserver
{
    //
    public function updated(UserBuyedTariff $buyedTariff)
    {
        if($buyedTariff->category === 'free' && $buyedTariff->free_download == $buyedTariff->tariff->free_download){
            $buyedTariff->status = 0;
            $buyedTariff->save();
        }
        if($buyedTariff->category !== 'free' && $buyedTariff->total_buy == $buyedTariff->tariff->total_buy){
            $buyedTariff->status = 0;
            $buyedTariff->save();
        }
    }
}
