<?php

namespace App\Observers;

use App\Models\Cart;
use App\Services\Contracts\CartServiceInterface;

class CartObserver
{
    protected $cartService;

    public function __construct(CartServiceInterface $cartService)
    {
        $this->cartService = $cartService;
    }

    public function creating(Cart $item)
    {
        $this->cartService->productAddingEvent($item);
    }
}
