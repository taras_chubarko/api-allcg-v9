<?php

namespace App\Observers;

use App\Models\UserBuyedProduct;

class UserBuyedProductObsever
{
    //
    public function created(UserBuyedProduct $buyedProduct)
    {
        //оновлюємо к-ть куплених товарів в тарифі користувача
        $user = $buyedProduct->user;
        $product = $buyedProduct->product;
        $buyedTarif = $user->buyedTariffs()->where('category', $product->price['category'])
            ->where('status', 1)
            ->first();
        if ($buyedTarif) {
            $buyedTarif->total_buy += 1;
            $buyedTarif->save();
        }
    }
}
