<?php

namespace App\Observers;

use App\Facades\OrderFacade;
use App\Models\Order;

class OrderObserver
{
    public function updated(Order $order)
    {
        //слухаємо зміну статусу
        if($order->getOriginal('status') == 0 && $order->status == 1){
            OrderFacade::saveProductToUser($order);
            OrderFacade::notify($order);
        }

    }
}
