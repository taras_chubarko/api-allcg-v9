<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();

        if ($user && $user->isAdmin()) {
            return $next($request);
        }

        if ($user && !$user->isAdmin()) {
            return response()->json(['error' => "You don't have access rights for this resource."], 403);
        }

        if (!$user) {
            return response()->json(['error' => "Unauthorized."], 401);
        }
    }
}
