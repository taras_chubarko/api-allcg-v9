<?php

namespace App\Providers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\ProductDownload;
use App\Models\User;
use App\Models\UserBuyedProduct;
use App\Models\UserBuyedTariff;
use App\Models\UserPrivate;
use App\Models\UserShop;
use App\Observers\CartObserver;
use App\Observers\OrderObserver;
use App\Observers\ProductDownloadObserver;
use App\Observers\UserBuyedProductObsever;
use App\Observers\UserBuyedTariffObserver;
use App\Observers\UserObserver;
use App\Observers\UserPrivateObserver;
use App\Observers\UserShopObserver;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $loader = AliasLoader::getInstance();
        //        $loader->alias(\Laravel\Sanctum\PersonalAccessToken::class, \App\Models\PersonalAccessToken::class);
        //
        Order::observe(OrderObserver::class);
        User::observe(UserObserver::class);
        UserShop::observe(UserShopObserver::class);
        UserPrivate::observe(UserPrivateObserver::class);
        ProductDownload::observe(ProductDownloadObserver::class);
        UserBuyedTariff::observe(UserBuyedTariffObserver::class);
        Cart::observe(CartObserver::class);
        UserBuyedProduct::observe(UserBuyedProductObsever::class);
    }
}
