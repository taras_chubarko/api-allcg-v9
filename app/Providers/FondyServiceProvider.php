<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FondyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Services\Contracts\FondyServiceInterface::class,
            \App\Services\FondyService::class,
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('fondy', \App\Services\FondyService::class);
    }
}
