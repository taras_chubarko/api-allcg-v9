<?php

namespace App\Services;

use App\Classes\Signature;
use App\Services\Contracts\FondyServiceInterface;
use Cloudipsp\Checkout;
use Cloudipsp\Configuration;
use Cloudipsp\Payment;

class FondyService implements FondyServiceInterface
{
    public function __construct()
    {
        Configuration::setMerchantId(config('fondy.merchant_id'));
        Configuration::setSecretKey(config('fondy.secret_key'));

        Signature::merchant(config('fondy.merchant_id'));
        Signature::password(config('fondy.secret_key'));
    }

    public function create(array $data): array
    {
        try{
            $checkoutData['order_id'] = $data['order_id'];
            $checkoutData['currency'] = 'USD';
            $checkoutData['amount'] = round($data['amount'] * 100);
            $checkoutData['sender_email'] = $data['sender_email'];
            $checkoutData['lang'] = 'ru';
            $checkoutData['server_callback_url'] = config('app.url') . '/fondy/callback';
            $token = Checkout::token($checkoutData)->getData();
            return $token;
        }catch (\Exception $exception){
            logger($exception->getMessage());
            abort(500, 'fondy.order.error');
        }
    }

    public function recurring(array $data): array
    {
        try{

            $recurringData['order_id'] = $data['order_id'];
            $recurringData['currency'] = 'USD';
            $recurringData['amount'] = round($data['amount'] * 100);
            $recurringData['rectoken'] = $data['rectoken'];

            $recurring_order = Payment::recurring($recurringData)->getData();
            return $recurring_order;
        }catch (\Exception $exception){
            logger($exception->getMessage());
            abort(500, 'fondy.recurring.error');
        }
    }

    public function verifyCard(): array
    {
        try{
            $faker = \Faker\Factory::create();

            $user = auth()->user();

            $data = [
                'verification_type' => 'code', //default - amount
                'currency' => 'USD',
                'amount' => 100,
                'verification' => 'Y',
                'order_id' => 'verifyCard-' . $faker->randomNumber(),
                'lang' => 'ua',
                //'server_callback_url' => config('app.url') . '/fondy/callback',
                'sender_email' => $user->email,
                'required_rectoken' => 'Y'
            ];
            $data = Checkout::token($data)->getData();

            return $data;
        }catch (\Exception $exception){
            logger($exception->getMessage());
            abort(500, 'fondy.verifyCard.error');
        }
    }
}
