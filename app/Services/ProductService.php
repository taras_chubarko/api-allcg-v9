<?php

namespace App\Services;

use App\Exceptions\ApiException;
use App\Exceptions\DownloadProductException;
use App\Models\CatalogItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\ProductDownload;
use App\Models\User;
use App\Models\UserBuyedProduct;
use App\Services\Contracts\ProductServiceInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Crypt;

class ProductService implements ProductServiceInterface
{
    public function rating(Product $product): array
    {
        return [
            'comments' => $product->comments ? $product->comments->count() : 0,
            'downloads' => $product->downloads ? $product->downloads->count() : 0,
            'likes' => $product->likes ? $product->likes->count() : 0,
            'rating' => $product->rate,
            'views' => $product->views ? $product->views->count() : 0,
        ];
    }

    public function previewImagePath(Product $product): string
    {
        $previews = 'images/noim.jpg';
        if (!empty($product->files['previews']))
            $previews = $product->files['previews'][0]['path'];
        return $previews;
    }

    public function formats(Product $product): array
    {
        $items = [];
        if (!empty($product->files['archives']))
            $formats = collect([]);
        foreach ($product->files['archives'] as $archive) {
            $formats->push($archive['formats']);
        }
        $items = $formats->collapse()->unique()->values()->toArray();
        return $items;
    }

    public function content(Product $product): string
    {
        $content = 'models';
        if (count($product->category) > 0) {
            $cat = CatalogItem::query()->find($product->category[0]);
            switch ($cat->slug) {
                case 'video':
                    $content = 'video';
                    break;
                case 'audio':
                    $content = 'audio';
                    break;
                default:
                    $content = 'models';
            }
        }
        return $content;
    }

    public function slides(Product $product): array
    {
        $slides = [];
        if (!empty($product->files['previews']))
            foreach ($product->files['previews'] as $preview) {
                $slides[] = $preview['path'];
            }
        return $slides;
    }

    public function lastProductsAndGallery(User $user): array
    {
        $products = Product::query()
            ->where('user_id', $user->id)
            ->take(4)
            ->select('files')
            ->get();
        $items = [];
        if ($products->count() > 0)
            foreach ($products as $item)
                $items[] = $item->files['previews'][0]['path'];
        return $items;
    }

    public function breadcrumbs(Product $product): object
    {
        $breadcrumbs = CatalogItem::query()->whereIn('_id', $product->category)->get();
        return $breadcrumbs;
    }

    public function download($crypt): string
    {
        try {
            $crypt = Crypt::decrypt($crypt);
            $user = User::query()->find($crypt['user_id']);
            $product = Product::query()->find($crypt['product_id']);
            if(!$product){
                abort(404, 'Такого товару не існує.');
            }
            $link = null;
            //Перевіряємо чи був куплений товар авторизованого користувача
            $userProductOrder = UserBuyedProduct::query()
                ->where('user_id', $user ? $user->id : null)
                ->where('product_id', $product->id)
                ->first();
            //Перевіряємо чи був куплений товар неавторизованого користувача
            $anonimOrders = Order::query()->where('ip', $crypt['ip'])->get();
            $anonimProductOrder = OrderItem::query()
                ->whereIn('order_id', $anonimOrders->pluck('id')->toArray())
                ->where('product_id', $product->id)
                ->first();


            if($userProductOrder
                || $anonimProductOrder
                || $this->checkIfDownloadFreeProduct($product, $user, $crypt['ip'])
                || $this->checkIfDownloadNotFreeProduct($product, $user, $crypt['ip'])){
                $link = storage_path('app/'.$product->files['archives'][0]['path']);
            }


            if ($link) {
                //return storage_path('app/'.$product->files['archives'][0]['path']);
                ProductDownload::query()->updateOrCreate([
                    'product_id' => $product->id,
                    'user_id' => $user ? $user->id : null,
                    'ip' => $crypt['ip'],
                ]);
                return $link;
            } else {
                abort(403, 'Ви не можете завантажити даний продукт, можливо дія вашого тарифу не дійсна, або ви не купували даний продукт.');
            }
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            throw new ApiException($exception->getMessage(), $exception->getStatusCode());
        }
    }

    //Якщо товар free або ціна = 0 перевіряємо чи може скачати товар
    public function checkIfDownloadFreeProduct($product, $user, $ip): bool
    {
        if ($product->price['category'] == 'free' || $product->price['amount'] == 0) {
            if(!$user){
                //Вибираємо скачані товари по ip
                $pdIp = ProductDownload::query()->with('product')
                    ->whereNull('user_id')
                    ->where('ip', $ip)->get();
                $pdIpCount = $pdIp->where('product.price.category', 'free')->count();
                if($pdIpCount < 3){
                    return true;
                }else{
                    abort(404, 'Анонім безкоштовно може завантажити лише 3 товари.');
                }
            }else{
                //Перевіряємо чи є куплений free тариф
                $freeTariff = $user->buyedTariffs()->where('category', 'free')
                    ->where('status', 1)
                    ->first();
                if($freeTariff){
                    //Якщо куплений тариф активний тоді звіряємо ліміти
                    if($freeTariff->free_download < $freeTariff->tariff->free_download){
                        return true;
                    }else{
                        abort(403, 'Ваш ліміт в тарифному план закінчився.');
                    }
                }else{
                    $pdIp = ProductDownload::query()->with('product')
                        ->where('user_id', $user->id)
                        ->get();
                    $pdIpCount = $pdIp->where('product.price.category', 'free')->count();
                    if($pdIpCount < 5){
                        return true;
                    }else{
                        abort(403, 'Авторирований користувач без тарифу безкоштовно може завантажити лише 5 товарів.');
                    }
                }
            }
        }
        return false;
    }

    //Якщо товар не free і є куплений тариф
    public function checkIfDownloadNotFreeProduct($product, $user, $ip): bool
    {
        if ($product->price['category'] !== 'free') {
            //Перевіряємо чи є куплений тариф
            $tariff = $user->buyedTariffs()->where('category', $product->price['category'])
                ->where('status', 1)
                ->first();
            if($tariff){
                if($tariff->free_download < $tariff->tariff->free_download){
                    return true;
                }else{
                    abort(403, 'Ваш ліміт безкоштовних завантажень в тарифному плані закінчився.');
                }
            }else{
                abort(403, 'Ваш тарифний план закінчився.');
            }
        }
        return false;
    }

    public function downloadOrBuy(Product $product): string
    {
        try {
            $user = auth()->user();
            $result = 'buy';

            if ($this->checkDownload($product, $user)) {
                $result = 'download';
            }

            return $result;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'product.downloadOrBuy.error');
        }
    }

    public function checkDownload(Product $product, $user, $ip = null): bool
    {
        try {
            $result = false;
            if ($user) {
                $tariff = $user->buyedTariffs()->where('category', $product->price['category'])
                    ->where('status', 1)
                    ->first();
                //Якщо є куплений тариф
                if ($tariff && $tariff->free_download < $tariff->tariff->free_download) {
                    $result = true;
                }
                //Якщо авторизований без тарифу
                if ($product->price['category'] == 'free' || $product->price['amount'] == 0) {
                    $result = true;
                }
            } else {
                if ($product->price['category'] == 'free' || $product->price['amount'] == 0) {
                    $result = true;
                }
            }

            return $result;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, $exception->getMessage());
        }
    }

    public function calcDownload(ProductDownload $productDownload): void
    {
        try {
            $product = $productDownload->product;
            $user = $productDownload->user;
            //Шукаємо активний тариф користувача
            if($user && $user->buyedTariffs()->count()){
                $tariff = $user->buyedTariffs()->where('category', $product->price['category'])
                    ->where('status', 1)
                    ->first();
                logger($tariff->tariff->free_download);
                if ($tariff && $tariff->free_download < $tariff->tariff->free_download) {
                    $tariff->free_download += 1;
                    $tariff->save();
                }
            }
        }catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, $exception->getMessage());
        }
    }

    public function priceAmount($price): float
    {
        $user = auth()->user();
        if($user &&
        $userTariff = $user->buyedTariffs()->where('category', $price['category'])
            ->where('status', 1)
            ->first()
        ){
            $discount = ($price['amount']*$userTariff->tariff->discount)/100;
            $amount = $price['amount'] - $discount;
            return (float) $amount;
        }
        return $price['amount'];
    }

    public function priceOldAmount($price): float
    {
        $user = auth()->user();
        if($user &&
            $userTariff = $user->buyedTariffs()->where('category', $price['category'])
                ->where('status', 1)
                ->first()
        ){
            return (float) $price['amount'];
        }
        return (float) 0;
    }
}
