<?php

namespace App\Services;

use App\Facades\FondyFacade;
use App\Models\ProductTariff;
use App\Models\SmsCode;
use App\Models\User;
use App\Models\UserBuyedTariff;
use App\Models\UserCard;
use App\Models\UserPhone;
use App\Rules\SmsCodeExistsRule;
use App\Services\Contracts\UserServiceInterface;
use Daaner\TurboSMS\Facades\TurboSMS;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class UserService implements UserServiceInterface
{
    public function rating(User $user): array
    {
        $faker = Faker::create();
        return [
            'followers' => $faker->numberBetween(0, 1000),
            'likes' => $faker->numberBetween(0, 1000),
            'rating' => $user->rate,
            'views' => $faker->numberBetween(0, 1000),
            'count_products' => $faker->numberBetween(10, 1000),
        ];
    }

    public function signUp(array $args): string
    {
        try {
            User::query()->create([
                'email' => $args['email'],
                'password' => $args['password'],
                'roles' => ['user'],
                'is_activate' => false,
                'rate' => 0,
            ]);
            //
            $isAuth = auth()->attempt([
                'email' => $args['email'],
                'password' => $args['password'],
            ]);
            //
            if (!$isAuth) {
                abort(401, 'Unauthorized');
            }
            $user = auth()->user();
            $token = $user->getToken();
            return $token;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.signup.error');
        }
    }

    public function signIn(array $args): string
    {
        try {
            $isAuth = auth()->attempt([
                'email' => $args['email'],
                'password' => $args['password'],
            ]);
            //
            if (!$isAuth) {
                abort(401, 'Unauthorized');
            }
            $user = auth()->user();
            $token = $user->getToken();
            return $token;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.signin.error');
        }
    }

    public function signInSoc(array $args): string
    {
        try {
            $user = User::query()->where('email', $args['email'])->first();
            if(!$user){
                abort(403, 'Такий користувач не зареєстрований.');
            }
            $isAuth = auth()->loginUsingId($user->id);
            //
            if (!$isAuth) {
                abort(401, 'Unauthorized');
            }
            $token = $user->getToken();
            return $token;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            api_exception($exception);
        }
    }

    public function signUpSoc(array $args): string
    {
        try {
            $data = json_decode($args['data']);
            $user = User::query()->where('email', $data->email)->first();
            if($user){
                abort(403, 'Такий користувач уже зареєстрований.');
            }

            $user = User::query()->create([
                'email' => $data->email,
                'password' => Str::random(8),
                'roles' => ['user'],
                'is_activate' => false,
                'rate' => 0,
            ]);

            $isAuth = auth()->loginUsingId($user->id);
            //
            if (!$isAuth) {
                abort(401, 'Unauthorized');
            }
            $token = $user->getToken();
            return $token;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            api_exception($exception);
        }
    }

    public function signInAdmin(array $args)
    {
        try {
            $isAuth = auth()->attempt([
                'email' => $args['email'],
                'password' => $args['password'],
            ]);

            if (!$isAuth) {
                abort(401, 'Unauthorized');
            }

            $user = auth()->user();

            if (!$user->isAdmin()) {
                abort(403, "You don't have access rights for this resource.");
            }

            $token = $user->getToken();
            return $token;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort($exception->getCode(), $exception->getMessage());
        }
    }

    public function sendSmsCode(array $args): bool
    {
        try {
            $code = rand(111111, 999999);

            SmsCode::query()->updateOrCreate([
                'phone' => $args['phone'],
            ], [
                'phone' => $args['phone'],
                'code' => $code,
            ]);

            $result = TurboSMS::sendMessages($args['phone'], 'Activation code: ' . $code);
            return $result['success'];
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.sendSmsCode.error');
        }
    }

    public function smsCodeActivation(array $args): bool
    {
        try {
            $user = auth()->user();
            $user->update([
                'is_activate' => true
            ]);
            //Зберігаємо номер телефону
            UserPhone::query()->updateOrCreate([
                'user_id' => $user->id,
                'phone' => $args['phone']
            ], [
                'user_id' => $user->id,
                'phone' => $args['phone']
            ]);
            //Видаляємо код активації
            SmsCode::query()->where('phone', $args['phone'])->delete();
            return true;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.sendSmsCode.error');
        }
    }

    public function setDefaultFields(User $user): void
    {
        try {

            $name = $user->id . '.png';
            $folder = $user->id;
            Storage::makeDirectory('public/avatar/' . $folder);
            \Avatar::create($user->full_name)->save(storage_path('app/public/avatar/' . $folder . '/' . $name));

            $user->update([
                'first_name' => null,
                'last_name' => null,
                'avatar' => 'avatar/' . $folder . '/' . $name,
                'bg' => 'user_bg/default.jpg',
                'address_id' => null,
                'birthday' => null,
                'experience' => null,
                'name' => null,
                'rate' => 0,
                'gender' => null,
                'roles2' => [],
                'software' => [],
                'specialization' => [],
            ]);
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.setDefaultFields.error');
        }
    }

    public function randomBg(User $user): string
    {
        try {
            $folder = 'public/user_bg/'.$user->id;
            if(!Storage::exists($folder)){
                Storage::makeDirectory($folder);
            }
            Storage::delete(Storage::allFiles($folder));

            $faker = Faker::create();
            $fileNum = $faker->numberBetween(1,10);
            $from = 'profile/random_bg/'.$fileNum.'.jpg';
            $to = $folder.'/'.$fileNum.'.jpg';

            Storage::copy($from, $to);

            $user->update([
                'bg' => 'user_bg/'.$user->id.'/'.$fileNum.'.jpg'
            ]);

            return $user->bg;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.randomBg.error');
        }
    }

    public function deleteBg(User $user): string
    {
        try {
            $folder = 'public/user_bg/'.$user->id;
            Storage::deleteDirectory($folder);

            $user->update([
                'bg' => 'user_bg/default.jpg'
            ]);

            return $user->bg;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.deleteBg.error');
        }
    }

    public function uploadBg(User $user, array $args): string
    {
        try {
            $folder = 'public/user_bg/'.$user->id;
            if(!Storage::exists($folder)){
                Storage::makeDirectory($folder);
            }
            Storage::delete(Storage::allFiles($folder));

            $file = $args['file'];
            $filePath = Storage::disk('public')->putFile('user_bg/'.$user->id, new File($file));

            $user->update([
                'bg' => $filePath
            ]);

            return $user->bg;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.uploadBg.error');
        }
    }

    public function uploadAvatar(User $user, array $args): string
    {
        try {
            $folder = 'public/avatar/'.$user->id;
            if(!Storage::exists($folder)){
                Storage::makeDirectory($folder);
            }
            Storage::delete(Storage::allFiles($folder));

            $file = $args['file'];
            $filePath = Storage::disk('public')->putFile('avatar/'.$user->id, new File($file));

            $user->update([
                'avatar' => $filePath
            ]);

            return $user->avatar;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.uploadAvatar.error');
        }
    }

    public function uploadDocs(User $user, array $args): string
    {
        try {
            $folder = 'public/user_docs/'.$user->id;
            if(!Storage::exists($folder)){
                Storage::makeDirectory($folder);
            }
            Storage::delete(Storage::allFiles($folder));

            $file = $args['file'];
            $filePath = Storage::disk('public')->putFile('user_docs/'.$user->id, new File($file));
            return $filePath;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.uploadDocs.error');
        }
    }

    public function saveCard(User $user, array $args): bool
    {
        try {
            $data = json_decode($args['data']);
            UserCard::query()->updateOrCreate([
                'user_id' => $user->id,
                'name' => $data->masked_card,
                'cardInfo' => $data,
                'payOut' => false,
            ],[
                'user_id' => $user->id,
                'name' => $data->masked_card,
                'cardInfo' => $data,
                'payOut' => false,
            ]);
            return  true;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.saveCard.error');
        }
    }

    public function buyedProducts(User $user, array $args): object
    {
        try {
            $items = $user->buyedProducts()->orderBy('created_at')
                ->paginate($args['per_page'], ['*'], 'page', $args['current_page']);
            if($items){
                $items->getCollection()->transform(function ($item){
                    return $item->product;
                });
            }
            return $items;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.buyedProducts.error');
        }
    }

    public function tariffs(User $user): Collection
    {
        try {
            $tariffs = ProductTariff::all();

            $tariffs->transform(function ($item, $key) use ($user) {
                $userTariff = $user->buyedTariffs()
                    ->where('tariff_id', $item->id)
                    ->where('status', 1)
                    ->first();

                $item['buyed'] = $userTariff ? true : false;
                $item['total'] = [
                    'total_buy' => $userTariff ? $userTariff->total_buy : 0,
                    'free_download' => $userTariff ? $userTariff->free_download : 0,
                ];
                return $item;
            });
            return $tariffs;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'user.tariffs.error');
        }
    }

    public function buyTariff(array $args): string
    {
        try {
            $data = json_decode($args['data'], true);
            $result = null;
            if(!empty($data['card'])){//Якщо оплата зі збереженої карти (рекурентний платіж)
                $card = UserCard::query()->find($data['card']);
                $fondy = FondyFacade::recurring([
                    'order_id' => 'tariff-'.Str::uuid(),
                    'amount' => $data['tariff']['amount'],
                    'rectoken' => $card->cardInfo['rectoken'],
                ]);
                //Вдалий платіж
                if($fondy['order_status'] === 'approved'){
                    $result = $this->saveTariff($data['tariff']);
                }else{
                    abort(500, 'Неможливо списати гроші з картки.');
                }
            }else{
                $result = $this->saveTariff($data['tariff']);
            }
            return $result;
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, $exception->getMessage());
        }
    }

    protected function saveTariff($tariff)
    {
        $user = auth()->user();
        $plan = UserBuyedTariff::query()->create([
            'user_id' =>  $user->id,
            'tariff_id' => $tariff['id'],
            'category' => $tariff['category'],
            'free_download' => 0,
            'total_buy' => 0,
            'status' => 1,
        ]);
        return $plan->id;
    }

    public function userProducts(User $user, array $args)
    {
        try {
            $orderBy = $args['orderBy'] ?: 'created_at';
            $folderName = $args['folderName'];
            $products = $user->products();
            if ($folderName) {
                $folder = $user->productsFolders()->where('name', $folderName);
                $product_ids = $folder->value('product_ids');
                $products = $products->whereIn('_id', $product_ids);
            }
            return $products
                ->orderBy($orderBy, 'desc')
                ->paginate($args['limit'], ['*'], 'page', $args['page']);
        }catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, $exception->getMessage());
        }
    }
}
