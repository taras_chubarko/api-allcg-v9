<?php

namespace App\Services;

use App\Exceptions\ApiException;
use App\Models\Cart;
use App\Models\Product;
use App\Services\Contracts\CartServiceInterface;
use Illuminate\Support\Facades\Validator;

class CartService implements CartServiceInterface
{
    public function addToCart(array $args): bool
    {
        try {
            $validated = Validator::make($args, [
                'visitor_id' => ['required', 'string'],
                'product_id' => ['required', 'string'],
            ])->validate();

            $product = Product::query()->find($validated['product_id']);
            $price = !empty($args['amount']) ? $args['amount'] : $product->getAmount();
            $old_amount = $product->getOldAmount();
            $message = null;

            Cart::query()->updateOrCreate([
                'instance' => $validated['visitor_id'],
                'product_id' => $product->id,
                'name' => $product->name,
                'qty' => 1,
                'price' => $price,
                'options' => [
                    'category' => $product->price['category'],
                    'old_amount' => $old_amount,
                    'message' => $message,
                ],
            ]);

            return true;

        } catch (\Exception $exception) {
            api_exception($exception);
        }
    }

    public function productAddingEvent(Cart $item): void
    {
        try{
            $cart = Cart::query()->instance($item->instance)->get();
            //групуємо товари в кошику по категоріям
            $groups = $cart->groupBy('options.category');
            if(!empty($groups[$item->options['category']])){
                //Визначаємо к-ть товарів категорії
                $count = $groups[$item->options['category']]->count();
                //Первіряємо чи є тариф у користувача в даній категорії і яка там куплена к-ть товару
               $user = auth()->user();
               if($user && $userTariff = $user->buyedTariffs()->where('category', $item->options['category'])
                       ->where('status', 1)
                       ->first()){
                   //Сумуємо к-ть товарів які уже куплені + товари категорії в кошику +1 поточний товар
                   //для якого будемо визначати ціну
                   $total_buy = $userTariff->total_buy + $count + 1;
                   if($total_buy >= $userTariff->tariff->total_buy){
                       $item->price = $item->product->getOldAmount();
                       $item->options = [
                           'category' => $item->product->price['category'],
                           'old_amount' => 0,
                           'message' => 'full_price_message',
                       ];
                   }
               }
            }
        }catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, $exception->getMessage());
        }
    }

    public function cart(array $args): array
    {
        try {
            $validated = Validator::make($args, [
                'visitor_id' => ['required', 'string'],
            ])->validate();

            $result['count'] = 0;
            $result['total'] = 0;
            $result['items'] = [];

            $cart = Cart::query()->instance($validated['visitor_id']);

            if($cart->count()){
                $result['count'] = $cart->count();
                $result['total'] = $cart->total();
                $result['items'] = $cart->get();
            }
            return $result;
        } catch (\Exception $exception) {
            api_exception($exception);
        }
    }
}
