<?php

namespace App\Services;

use App\Facades\FondyFacade;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Models\UserBuyedProduct;
use App\Models\UserCard;
use App\Notifications\OrderNotification;
use App\Services\Contracts\OrderServiceInterface;

class OrderService implements OrderServiceInterface
{
    public function make(array $args)
    {
        try {
            $prefix = config('app.order_prefix');
            $items = json_decode($args['items'], true);
            $order = Order::query()->create([
                'number' => config('order.start_from_num'),
                'total' => $args['amount'],
                'user_id' => $this->userUID($args['email']),
                'email' => $args['email'],
                'status' => 0,
                'ip' => $args['ip'],
            ]);
            foreach ($items as $item) {
                $item['order_id'] = $order->id;
                unset($item['id']);
                OrderItem::query()->create($item);
            }
            //генерація токену на оплату fondy
            if(!$args['card_id'])
            {
                $data = FondyFacade::create([
                    'order_id' => $prefix . $order->number,
                    'amount' => $args['amount'],
                    'sender_email' => $args['email'],
                ]);
                //
                return $data['response_status'] === 'success' ? $data['token'] : null;
            }else{
                $card = UserCard::query()->find($args['card_id']);

                $data = FondyFacade::recurring([
                    'order_id' => $prefix . $order->number,
                    'amount' => $args['amount'],
                    'sender_email' => $args['email'],
                    'rectoken' => $card->cardInfo['rectoken'],
                ]);

                return json_encode($data);
            }

            return null;

        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'order.create.error');
        }
    }

    protected function userUID($email = null)
    {
        $uid = null;
        $user = auth()->user();
        if ($user)
            $uid = $user->id;
        else
            $user = User::query()->where('email', $email)->first();
        if ($user)
            $uid = $user->id;
        return $uid;
    }

    public function complete(array $args)
    {
        try {
            $prefix = config('app.order_prefix');
            $orderId = (int)str_replace($prefix, '', $args['order_id']);
            //ставим статус замовлення завершено
            $order = Order::query()->where('number', $orderId)->first();
            $order->update(['status' => 1]);
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'order.complete.error');
        }
    }

    public function saveProductToUser(Order $order)
    {
        try {
            if ($order->user_id) {
                $order->products()->each(function ($product) use ($order) {
                    UserBuyedProduct::query()
                        ->updateOrCreate([
                            'user_id' => $order->user_id,
                            'product_id' => $product->product_id,
                        ], [
                            'user_id' => $order->user_id,
                            'product_id' => $product->product_id,
                        ]);
                });
                $first = $order->products()->first();
                //Чистимо кошик
                Cart::query()->instance($first->instance)->delete();
            }
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'order.saveProductToUser.error');
        }
    }

    public function notify(Order $order)
    {
        try {
            $order->notify(new OrderNotification());
        }catch (\Exception $exception) {
            logger($exception->getMessage());
            abort(500, 'order.notify.error');
        }
    }

}
