<?php

namespace App\Services\Contracts;

use App\Models\CatalogItem;
use App\Models\Product;
use App\Models\ProductDownload;
use App\Models\User;

interface ProductServiceInterface
{
    //рейтинг товару
    public function rating(Product $product): array;
    //картинка превю товару
    public function previewImagePath(Product $product): string;
    //формати файлів в архіві
    public function formats(Product $product): array;
    //тип контенту товару
    public function content(Product $product): string;
    //слайди товару
    public function slides(Product $product): array;
    //галерея товарів в топ користувачах
    public function lastProductsAndGallery(User $user): array;
    //крошки
    public function breadcrumbs(Product $product): object;
    //Скачування купленого товару
    public function download($crypt): string;
    //Встановлення кнопки закантачння чи покупки товару
    public function downloadOrBuy(Product $product): string;
    //Перевірка можливості скачати товар
    public function checkDownload(Product $product, $user, $ip): bool;
    //Калькуляція кількості скачувань в тарифному плані користувача
    public function calcDownload(ProductDownload $productDownload): void;
    //Ціна товару в тарифному плані
    public function priceAmount($price): float;
    //Ціна товару без тарифного плану
    public function priceOldAmount($price): float;
}
