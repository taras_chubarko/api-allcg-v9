<?php

namespace App\Services\Contracts;

use App\Models\Cart;

interface CartServiceInterface
{
    //Додати товар в кошик
    public function addToCart(array $args): bool;
    //Валідація товару на правильність ціни в тарифі, при додавані в кошик
    public function productAddingEvent(Cart $item): void;
    //Кошик користувача
    public function cart(array $args): array;
}
