<?php

namespace App\Services\Contracts;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface UserServiceInterface
{
    //рейтинг користувача
    public function rating(User $user): array;
    //реєстрація
    public function signUp(array $args): string;
    //авторизація
    public function signIn(array $args): string;
    //Авторизація через соцмережі
    public function signInSoc(array $args): string;
    //Реєстрація по соціальній мережі
    public function signUpSoc(array $args): string;
    //Відправка коду активації користувача по смс
    public function sendSmsCode(array $args): bool;
    //Активація користувача через смс код
    public function smsCodeActivation(array $args): bool;
    //Створення дефолтних значень користувача
    public function setDefaultFields(User $user): void;
    //Рандомне фонове зображення користувача
    public function randomBg(User $user): string;
    //Видалення фонового зображення користувача
    public function deleteBg(User $user): string;
    //Завантаження фонового зображення користувача
    public function uploadBg(User $user, array $args): string;
    //Завантаження аватарки користувача
    public function uploadAvatar(User $user, array $args): string;
    //Завантаження документів користувача
    public function uploadDocs(User $user, array $args): string;
    //Збереження банківської картки користувача
    public function saveCard(User $user, array $args): bool;
    //Куплені товари
    public function buyedProducts(User $user, array $args): object;
    //Тарифні плани користувача
    public function tariffs(User $user): Collection;
    //Купівля тарифного плану
    public function buyTariff(array $args): string;
    //Товари користувача
    public function userProducts(User $user, array $args);

}
