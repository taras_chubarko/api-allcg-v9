<?php

namespace App\Services\Contracts;

interface FondyServiceInterface
{
    public function create(array $data): array;

    public function recurring(array $data): array;

    public function verifyCard(): array;
}
