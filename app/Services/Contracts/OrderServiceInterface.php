<?php

namespace App\Services\Contracts;

use App\Models\Order;

interface OrderServiceInterface
{
    //створення заказу
    public function make(array $args);
    //завершення
    public function complete(array $args);
    //збереження куплених товарів в лк користувача
    public function saveProductToUser(Order $order);
    //відправка повідомлення на почту після завершення замовлення
    public function notify(Order $order);
}
