<?php

namespace App\Events\User\Profile\Chat;

use App\Models\User;
use App\Models\User\Profile\Chat\ChatMessage;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewChatMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public ChatMessage $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel('profile-chats.' . $this->message->chat_id);
    }

    public function broadcastWith()
    {
        $sender = $this->message->sender;
        $attachments = [];

        foreach ($this->message->attachments as $attachment) {
            $tempAttachment = $attachment;
            $tempAttachment['download_link'] = route('download.chat_attachments', ['attachment_id' => $attachment->id]);
            $tempAttachment['id'] = $attachment->id;
            unset($tempAttachment['_id']);
            $attachments[] = $tempAttachment;
        }
        return [
            'message' => [
                'id' => $this->message->id,
                'sent_at' => $this->message->sent_at,
                'status' => $this->message->status,
                'text' => $this->message->text,
                'sender' => [
                    'id' => $sender->id,
                    'full_name' => $sender->getFullNameAttribute()
                ],
                'attachments' => $attachments,
            ]
        ];
    }
}
