<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ChatMessageFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'message';
    }
}
