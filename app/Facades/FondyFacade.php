<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class FondyFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'fondy';
    }
}
