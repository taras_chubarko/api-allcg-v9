<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class GeoRU extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'geo_ru';

    protected $guarded = ['_id'];
}
