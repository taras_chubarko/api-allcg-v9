<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class UserBuyedTariff extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'users_buyed_tariff';

    protected $guarded = ['_id'];

    public function tariff()
    {
        return $this->belongsTo(ProductTariff::class, 'tariff_id');
    }
}
