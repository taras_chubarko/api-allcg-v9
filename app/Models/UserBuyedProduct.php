<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class UserBuyedProduct extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'users_buyed_products';

    protected $guarded = ['_id'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
