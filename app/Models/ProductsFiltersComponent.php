<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * @property-read string $id
 * @property string $component
 * @property-read ProductsFiltersField[] $fields
 * @property-read ProductComponent[] $productComponents
 */
class ProductsFiltersComponent extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'products_filters_components';

    protected $guarded = ['_id'];

    protected $fillable = ['component', 'fields_ids'];

    protected $attributes = [
        'fields_ids' => [],
    ];

    /**
     * @return CatalogItem[]
     */
    public function getCatalogCategories(): iterable
    {
        $catalogItemsIds = ProductComponent::query()->where('component', $this->component)
            ->get()->pluck('category_id')->all();

        return CatalogItem::query()->whereIn('_id', $catalogItemsIds)->get();
    }

    public function productComponents()
    {
        return $this->hasMany(
            ProductComponent::class,
            'component',
            'component'
        );
    }

    public function fields()
    {
        return $this->belongsToMany(
            ProductsFiltersField::class,
            null,
            'components_ids',
            'fields_ids'
        );
    }
}
