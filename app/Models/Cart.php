<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'cart';

    protected $guarded = ['_id'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function scopeInstance($query, $instance)
    {
        return $query->where('instance', $instance);
    }

    public function scopeTotal($query)
    {
        $items = $query->get();
        return $items->sum('price');
    }
}
