<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class UserPhone extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'user_phones';

    protected $guarded = ['_id'];

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = str_replace('+', '', $value);
    }
}
