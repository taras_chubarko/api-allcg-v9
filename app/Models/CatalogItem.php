<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class CatalogItem extends Model
{
    use HasFactory, Sluggable;

    protected $connection = 'mongodb';

    protected $collection = 'catalog_items';

    protected $guarded = ['_id'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    //
    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', '_id');
    }
    //
    public function parent()
    {
        return $this->hasOne(self::class, '_id', 'parent_id');
    }
    //
    public function breadcrumbs(): array
    {
        $items = [];
        if(is_null($this->parent_id)){
            $items[] = $this;
        }else{
            if($this->parent->parent){
                $items[] = $this->parent->parent;
            }
            if($this->parent){
                $items[] = $this->parent;
            }
            $items[] = $this;
        }
        //
        return $items;
    }
    //
    protected function sort(): Attribute
    {
        return Attribute::make(
            set: fn($value) => (int) $value
        );
    }
    //
    protected function status(): Attribute
    {
        return Attribute::make(
            set: fn($value) => (boolean) $value
        );
    }
}
