<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class ProductView extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'products_view';

    protected $guarded = ['_id'];
}
