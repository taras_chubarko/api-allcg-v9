<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class GeoUK extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'geo_uk';

    protected $guarded = ['_id'];
}
