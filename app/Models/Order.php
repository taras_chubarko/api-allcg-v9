<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use HasFactory, Notifiable;

    protected $connection = 'mongodb';

    protected $collection = 'orders';

    protected $guarded = ['_id'];

    public function setNumberAttribute($value)
    {
        $item  = self::query()->orderByDesc('number')->first();
        if(!$item){
            $this->attributes['number'] = $value;
        }else{
            $this->attributes['number'] = $item->number + 1;
        }
    }

    public function products()
    {
        return $this->hasMany(OrderItem::class);
    }
}
