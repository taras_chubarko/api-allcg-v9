<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class UserCard extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'users_card';

    protected $guarded = ['_id'];
}
