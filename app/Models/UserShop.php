<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class UserShop extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'user_shop';

    protected $guarded = ['_id'];

    protected $casts = [
        'scan_inn_verify_at' => 'datetime:Y-m-d H:i',
        'scan_passport_at' => 'datetime:Y-m-d H:i',
    ];
}
