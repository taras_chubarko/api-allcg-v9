<?php

namespace App\Models;

use App\Facades\ProductFacade;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use HasFactory, Sluggable;

    protected $connection = 'mongodb';

    protected $collection = 'products';

    protected $guarded = ['_id'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getRatingAttribute()
    {
        return ProductFacade::rating($this);
    }

    public function getPreviewImagePathAttribute()
    {
        return ProductFacade::previewImagePath($this);
    }

    public function getFormatsAttribute()
    {
        return ProductFacade::formats($this);
    }


    public function getContentAttribute()
    {
        return ProductFacade::content($this);
    }

    public function getDownloadOrBuyAttribute()
    {
        return ProductFacade::downloadOrBuy($this);
    }

    public function getSlidesAttribute()
    {
        return ProductFacade::slides($this);
    }

    public function getBreadcrumbsAttribute()
    {
        return ProductFacade::breadcrumbs($this);
    }

    public function owner()
    {
        return $this->hasOne(User::class, '_id', 'user_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'entity_id', 'id')
            ->where('entity', '=', 'product');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'entity_id', 'id')
            ->where('entity', '=', 'product');
    }

    public function likes()
    {
        return $this->hasMany(Likes::class, 'entity_id', 'id')
            ->where('entity', '=', 'product');
    }

    public function views()
    {
        return $this->hasMany(ProductView::class, 'product_id', 'id');
    }

    public function downloads()
    {
        return $this->hasMany(ProductDownload::class, 'product_id', 'id');
    }

    public function scopeStatus($query, $status = 1)
    {
        return $query->where('status', $status);
    }

    public function getAmount()
    {
        return ProductFacade::priceAmount($this->price);
    }

    public function getOldAmount()
    {
        return ProductFacade::priceOldAmount($this->price);
    }




}
