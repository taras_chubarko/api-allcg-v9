<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class GeoEN extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'geo_en';

    protected $guarded = ['_id'];
}
