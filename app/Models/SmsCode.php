<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class SmsCode extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'sms_codes';

    protected $guarded = ['_id'];
}
