<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class ProductTariff extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'products_tariff';

    protected $guarded = ['_id'];
}
