<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'comments';

    protected $guarded = ['_id'];

    //
    public function user()
    {
        return $this->hasOne(User::class, '_id', 'user_id');
    }

    public function likes()
    {
        return $this->hasMany(Likes::class, 'entity_id', 'id')
            ->where('entity', '=', 'comment');
    }

    public function getCountLikesAttribute()
    {
        return $this->likes->count();
    }
}
