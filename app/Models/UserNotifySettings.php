<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class UserNotifySettings extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'user_notify_settings';

    protected $guarded = ['_id'];
}
