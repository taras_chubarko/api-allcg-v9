<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * @property-read string $id
 * @property-read ProductComponent[] $productComponents
 * @property-read ProductsFiltersComponent[] $components
 * @property object $name
 * @property string $type
 * @property string $slug
 * @property CatalogItem $catalogItem
 */
class ProductsFiltersField extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'products_filters_fields';

    protected $guarded = ['_id'];

    protected $fillable = ['catalog_item_id', 'name', 'type', 'slug', 'components_ids'];

    protected $attributes = [
        'components_ids' => [],
        'catalog_item_id' => null,
    ];

    public function components()
    {
        return $this->belongsToMany(
            ProductsFiltersComponent::class,
            null,
            'fields_ids',
            'components_ids'
        );
    }

    public function catalogItem()
    {
        return $this->belongsTo(CatalogItem::class, 'catalog_item_id');
    }

    public function getSelectItems() {
        if ($this->catalogItem) {
            return $this->catalogItem->children;
        }
        return null;
    }
}
