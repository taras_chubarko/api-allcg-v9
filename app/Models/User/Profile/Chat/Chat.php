<?php

namespace App\Models\User\Profile\Chat;

use App\Models\User;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Relations\BelongsToMany;
use Jenssegers\Mongodb\Relations\HasMany;
use Jenssegers\Mongodb\Relations\HasOne;

class Chat extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'chats';

    protected $primaryKey = '_id';

    protected $fillable = [
        'name',
        'nickname',
        'creator_id',
        'avatar',
        'type',
    ];

    public function messages(): HasMany {
        return $this->hasMany(ChatMessage::class);
    }

    public function members(): BelongsToMany {
        return $this->belongsToMany(User::class, null, 'chat_ids', 'user_ids');
    }

    public function creator(): HasOne {
        return $this->hasOne(User::class, '_id', 'creator_id');
    }

    /**
     * Check if user belongs to this chat instance
     *
     * @param string $userId The id of user to check
     *
     * @return bool
     */
    public function hasUser(string $userId): bool
    {
        $user = $this->members()->find($userId);
        return (bool) $user;
    }
}
