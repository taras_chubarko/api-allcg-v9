<?php

namespace App\Models\User\Profile\Chat;

use Jenssegers\Mongodb\Eloquent\Model;

class ChatMessageAttachment extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'chat_message_attachments';

    protected $primaryKey = '_id';

    protected $fillable = [
        'type', 'size', 'name', 'path', 'extension', 'chat_message_id'
    ];

    public function message()
    {
        return $this->belongsTo(ChatMessage::class, 'chat_message_id');
    }
}
