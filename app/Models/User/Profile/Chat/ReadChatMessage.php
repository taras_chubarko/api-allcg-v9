<?php

namespace App\Models\User\Profile\Chat;

use App\Models\User;
use Jenssegers\Mongodb\Eloquent\Model;

class ReadChatMessage extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'read_chat_messages';

    protected $primaryKey = '_id';
    protected $fillable = ['chat_message_id', 'read_by_id'];

    public function message()
    {
        return $this->belongsTo(ChatMessage::class, 'chat_message_id');
    }

    public function readBy()
    {
        return $this->belongsTo(User::class, 'read_by_id');
    }
}
