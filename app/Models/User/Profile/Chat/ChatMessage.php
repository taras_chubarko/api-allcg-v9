<?php

namespace App\Models\User\Profile\Chat;

use App\Models\User;
use Jenssegers\Mongodb\Eloquent\Model;

class ChatMessage extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'chats_messages';

    protected $primaryKey = '_id';

    protected $fillable = [
        'text', 'chat_id', 'sender_id'
    ];

    protected $appends = ['status', 'sentAt'];

    public function chat()
    {
        return $this->belongsTo(Chat::class, 'chat_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function read()
    {
        return $this->hasMany(ReadChatMessage::class);
    }

    public function attachments() {
        return $this->hasMany(ChatMessageAttachment::class);
    }

    public function getStatusAttribute(): string
    {
        return !!count($this->read) ? 'read' : 'sent';
    }

    public function getSentAtAttribute(): string
    {
        return $this->created_at;
    }

    // region Class Functions

    /**
     * Get info about had current user read the message or not.
     *
     */
    public function markAsRead() {
        $user = auth()->user();
        return $this->read()->create(['read_by_id' => $user->id]);
    }

    // endregion
}
