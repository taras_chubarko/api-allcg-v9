<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

/**
 * @property-read string $id
 * @property string $component
 * @property-read CatalogItem $category
 */
class ProductComponent extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'product_category_component';

    protected $guarded = ['_id'];

    public function category()
    {
        return $this->hasOne(CatalogItem::class, '_id', 'category_id');
    }
}
