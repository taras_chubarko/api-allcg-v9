<?php

namespace App\Models;

use App\Facades\ProductFacade;
use App\Facades\UserFacade;
use App\Models\User\Profile\Chat\Chat;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, Sluggable;

    protected $connection = 'mongodb';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = ['_id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_activity' => 'datetime'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'full_name'
            ]
        ];
    }

    public function getIsOnlineAttribute()
    {
        return $this->last_activity > Carbon::now()->subMinutes(5)->format('Y-m-d H:i:s');
    }

    public function getRatingAttribute()
    {
        return UserFacade::rating($this);
    }

    public function getFullNameAttribute()
    {
        $name = [];
        if (!empty($this->first_name)) {
            $name[] = $this->first_name;
        }
        if (!empty($this->last_name)) {
            $name[] = $this->last_name;
        }
        if (count($name)) {
            return implode(' ', $name);
        }
        $m = explode('@', $this->email);
        return $m[0];
    }

    public function getLastProductsAndGalleryAttribute()
    {
        return ProductFacade::lastProductsAndGallery($this);
    }

    public function getCreatedAttribute()
    {
        return $this->created_at->format('d.m.Y');
    }

    public function isAdmin()
    {
        $roles = collect($this->roles);
        return $roles->contains('admin');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function chats() {
        return $this->belongsToMany(Chat::class, null, 'user_ids', 'chat_ids');
    }

    public function buyedTariffs()
    {
        return $this->hasMany(UserBuyedTariff::class, 'user_id', '_id');
    }

    public function notifySettings()
    {
        return $this->hasOne(UserNotifySettings::class);
    }

    public function shop()
    {
        return $this->hasOne(UserShop::class);
    }

    public function private()
    {
        return $this->hasOne(UserPrivate::class);
    }

    public function cards()
    {
        return $this->hasMany(UserCard::class);
    }

    public function buyedProducts()
    {
        return $this->hasMany(UserBuyedProduct::class);
    }

    public function plan()
    {
        $plan = 'world';
        if (!empty($_SERVER['HTTP_LANG'])) {
            switch ($_SERVER['HTTP_LANG']) {
                case 'uk':
                    $plan = 'sng';
                    break;
                case 'ru':
                    $plan = 'sng';
                    break;
                case 'en':
                    $plan = 'world';
                    break;
            }
        }
        return $plan;
    }

    public function getToken()
    {
        return Crypt::encrypt($this->id);
    }

    public function productsFolders()
    {
        return $this->hasMany(UserProductsFolder::class, 'user_id', '_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'user_id', '_id');
    }
}
