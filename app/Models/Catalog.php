<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Catalog extends Model
{
    use HasFactory, Sluggable;

    protected $connection = 'mongodb';

    protected $collection = 'catalog';

    protected $guarded = ['_id'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function parentItems()
    {
        return $this->hasMany(CatalogItem::class, 'catalog_id', '_id')
            ->where('parent_id', null);
    }

}
