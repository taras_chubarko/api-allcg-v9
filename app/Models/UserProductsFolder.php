<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class UserProductsFolder extends Model
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $collection = 'user_products_folders';

    protected $guarded = ['_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
