<?php
/*
 * dd() with headers
 */

use App\Exceptions\ApiException;

if (!function_exists('ddh')) {
    function ddh($var){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Access-Control-Allow-Headers: *');
        dd($var);
    }
}

if (!function_exists('api_exception')) {
    function api_exception($exception){
        logger($exception->getMessage());
        if (method_exists($exception, 'getCode') && $exception->getCode() === 0) {
            throw new ApiException($exception->getMessage(), 500);
        }
        if (method_exists($exception, 'getStatusCode')) {
            throw new ApiException($exception->getMessage(), $exception->getStatusCode());
        }
        if (!empty($exception->status)) {
            throw new ApiException($exception->getMessage(), $exception->status);
        }
    }
}


