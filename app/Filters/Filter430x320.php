<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Filter430x320 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(430, 320);
    }
}
