<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Filter250x180 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(250, 180);
    }
}
