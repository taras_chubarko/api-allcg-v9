<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Filter485x315 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(485, 315);
    }
}
