<?php

namespace App\Filters;
use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Filter42x42 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(42, 42);
    }
}
