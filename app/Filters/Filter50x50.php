<?php

namespace App\Filters;
use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Filter50x50 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(50, 50);
    }
}
