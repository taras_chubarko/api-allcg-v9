<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Other;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class LabelValueType extends GraphQLType
{
    const NAME = 'LabelValueType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'label' => ['type' => Type::string()],
            'value' => ['type' => Type::string()],
        ];
    }
}
