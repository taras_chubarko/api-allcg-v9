<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Comments;

use App\GraphQL\Types\User\UserType;
use Carbon\Carbon;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CommentType extends GraphQLType
{
    const NAME = 'CommentType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'comment' => ['type' => Type::nonNull(Type::string())],
            'user' => ['type' => GraphQL::type(UserType::NAME)],
            'countLikes' => ['type' => Type::int()],
            'created_at' => [
                'type' => Type::nonNull(Type::string()),
                'args' => [
                    'format' => [
                        'type' => Type::nonNull(Type::string()),
                    ]
                ]
            ],
        ];
    }

    /* public function resolveCreatedAtField
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function resolveCreatedAtField($root, $args)
    {
        return Carbon::parse($root->created_at)->format($args['format']);
    }
}
