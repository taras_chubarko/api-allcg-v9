<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Geo;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class LocationType extends GraphQLType
{
    const NAME = 'LocationType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::id()],
            'city' => ['type' => Type::string()],
            'city_ascii' => ['type' => Type::string()],
            'lat' => ['type' => Type::float()],
            'lng' => ['type' => Type::float()],
            'country' => ['type' => Type::string()],
            'iso2' => ['type' => Type::string()],
            'iso3' => ['type' => Type::string()],
            'admin_name' => ['type' => Type::string()],
            'capital' => ['type' => Type::string()],
            'population' => ['type' => Type::string()],
        ];
    }
}
