<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Reviews;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ReviewFilesType extends GraphQLType
{
    const NAME = 'ReviewFilesType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'name' => ['type' => Type::string()],
            'path' => ['type' => Type::string()],
            'size' => ['type' => Type::int()],
            'type' => ['type' => Type::string()],
        ];
    }
}
