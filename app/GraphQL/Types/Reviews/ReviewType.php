<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Reviews;

use App\GraphQL\Types\User\UserType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ReviewType extends GraphQLType
{
    const NAME = 'ReviewType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'comment' => ['type' => Type::nonNull(Type::string())],
            'user' => ['type' => GraphQL::type(UserType::NAME)],
            'countLikes' => ['type' => Type::int()],
            'rating' => ['type' => Type::int(),],
            'created_at' => [
                'type' => Type::nonNull(Type::string()),
                'args' => [
                    'format' => [
                        'type' => Type::nonNull(Type::string()),
                    ]
                ]
            ],
            'files' => ['type' => Type::listOf(GraphQL::type(ReviewFilesType::NAME))],
        ];
    }
}
