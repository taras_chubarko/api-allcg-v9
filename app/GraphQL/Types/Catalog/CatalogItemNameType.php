<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Catalog;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CatalogItemNameType extends GraphQLType
{
    const NAME = 'CatalogItemNameType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'en' => ['type' => Type::string()],
            'ru' => ['type' => Type::string()],
            'uk' => ['type' =>Type::string()],
        ];
    }
}
