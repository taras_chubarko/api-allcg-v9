<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Catalog;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CatalogType extends GraphQLType
{
    const NAME = 'CatalogType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
            'slug' => ['type' => Type::string()],
        ];
    }
}
