<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Catalog;

use App\Models\CatalogItem;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CatalogItemType extends GraphQLType
{
    const NAME = 'CatalogItemType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type describing Catalog Item model.'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'catalog_id' => ['type' => Type::string()],
            'parent_id' => ['type' => Type::string()],
            'name' => ['type' => GraphQL::type(CatalogItemNameType::NAME)],
            'ico' => ['type' => Type::string()],
            'slug' => ['type' => Type::string()],
            'children' => ['type' => Type::listOf(GraphQL::type(CatalogItemType::NAME))],
            'countChild' => ['type' => Type::int()],
            'sort' => ['type' => Type::int()],
            'status' => ['type' => Type::boolean()],
            'breadcrumbs' => ['type' => Type::listOf(GraphQL::type(CatalogItemType::NAME))],
            'parent' => ['type' => GraphQL::type('CatalogItemType')],
            'ancestors' => ['type' => Type::listOf(GraphQL::type('CatalogItemType'))],
        ];
    }

    public function resolveParentField($root)
    {
        return CatalogItem::where('_id', $root->parent_id)->first();
    }

    public function resolveAncestorsField($root)
    {
        $ancestors = collect([]);
        $catalogItem = CatalogItem::where('_id', $root->parent_id)->first();
        while($catalogItem) {
            $ancestors->push($catalogItem);
            $catalogItem = CatalogItem::where('_id', $catalogItem->parent_id)->first();
        }
        return $ancestors;
    }
}
