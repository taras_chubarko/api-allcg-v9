<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User\Profile\Chat;

use App\GraphQL\Enums\Chat\ChatTypeEnum;
use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ChatType extends GraphQLType
{
    protected $attributes = [
        'name' => 'ChatType',
        'description' => 'A type for users chat.'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
            ],
            'type' => [
                'type' => GraphQL::type(ChatTypeEnum::NAME)
            ],
            'members' => [
                'type' => Type::listOf(GraphQL::type('UserType')),
            ],
            'name' => [
                'type' => Type::string()
            ],
            'nickname' => [
                'type' => Type::string()
            ],
            'creator' => [
                'type' => GraphQL::type('UserType'),
            ],
            'avatar' => [
                'type' => Type::string(),
            ],
            'messages' => [
                'type' => Type::listOf(GraphQL::type('ChatMessageType')),
            ],
            'last_message' => [
                'type' => GraphQL::type('ChatMessageType'),
            ],
            'unread_count' => [
                'type' => Type::int(),
            ]
        ];
    }

    protected function resolveAvatarField($root)
    {
        $user = auth()->user();
        if ($root->type === ChatTypeEnum::VALUES['DIALOG']) {
            return $root->members->where('id', '!==', $user->id)->first()->avatar;
        }

        return $root->avatar;
    }

    protected function resolveNameField($root)
    {
        $user = auth()->user();
        if ($root->type === ChatTypeEnum::VALUES['DIALOG']) {
            return $root->members->where('id', '!==', $user->id)->first()->full_name;
        }

        return $root->name;
    }

    protected function resolveLastMessageField($root)
    {
        if ($root->messages->count() < 1) {
            return null;
        }

        return $root->messages->last();
    }

    protected function resolveMembersField($root)
    {
        $user = auth()->user();
        return $root->members->where('id', '!==', $user->id);
    }

    protected function resolveMessagesField($root)
    {
        return $root->messages->sortByDesc('created_at');
    }

    protected function resolveUnreadCountField($root)
    {
        $user = auth()->user();
        return $root->messages
            ->where('sender_id', '!==', $user->id)
            ->filter(function ($message) {
                return !count($message->read);
            })
            ->count();
    }
}
