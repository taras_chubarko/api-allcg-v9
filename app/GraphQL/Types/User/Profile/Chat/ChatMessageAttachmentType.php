<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User\Profile\Chat;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ChatMessageAttachmentType extends GraphQLType
{
    protected $attributes = [
        'name' => 'ChatMessageAttachmentType',
        'description' => 'A type which describes chat message attachment model.'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
            ],
            'path' => [
                'type' => Type::string(),
            ],
            'name' => [
                'type' => Type::string(),
            ],
            'extension' => [
                'type' => Type::string(),
            ],
            'size' => [
                'type' => Type::string(),
            ],
            'type' => [
                'type' => Type::string(),
            ],
            'download_link' => [
                'type' => Type::string(),
            ],
        ];
    }

    protected function resolveDownloadLinkField($root)
    {
        return route('download.chat_attachments', ['attachment_id' => $root->id]);
    }
}
