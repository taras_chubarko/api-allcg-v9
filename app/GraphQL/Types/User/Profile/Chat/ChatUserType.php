<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User\Profile\Chat;

use Rebing\GraphQL\Support\Type as GraphQLType;

class ChatUserType extends GraphQLType
{
    protected $attributes = [
        'name' => 'User/Profile/Chat/ChatUser',
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [

        ];
    }
}
