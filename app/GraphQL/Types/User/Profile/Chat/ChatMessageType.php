<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User\Profile\Chat;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ChatMessageType extends GraphQLType
{
    protected $attributes = [
        'name' => 'ChatMessageType',
        'description' => 'A type which describes chat messages model.'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
            ],
            'text' => [
                'type' => Type::string(),
            ],
            'sender' => [
                'type' => GraphQL::type('UserType'),
            ],
            'chat' => [
                'type' => GraphQL::type('ChatType'),
            ],
            'sent_at' => [
                'type' => Type::string(),
            ],
            'status' => [
                'type' => Type::string(),
            ],
            'attachments' => [
                'type' => Type::listof(GraphQL::type('ChatMessageAttachmentType'))
            ]
        ];
    }
}
