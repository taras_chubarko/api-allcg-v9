<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserCardType extends GraphQLType
{
    const NAME = 'UserCardType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
            'cardInfo' => ['type' => GraphQL::type(UserCardInfoType::NAME)],
            'payOut' => ['type' => Type::boolean()],
        ];
    }
}
