<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use App\GraphQL\Types\Product\ProductType;
use App\Models\Product;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserProductsFolderType extends GraphQLType
{
    const NAME = 'UserProductsFolderType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
        ];
    }

}
