<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserShopType extends GraphQLType
{
    const NAME = 'UserShopType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'company_name' => ['type' => Type::string()],
            'vat' => ['type' => Type::string()],
            'edprpou' => ['type' => Type::string()],
            'scan_inn' => ['type' => Type::string()],
            'scan_passport' => ['type' => Type::string()],
            'scan_inn_verify_at' => ['type' => Type::string()],
            'scan_passport_at' => ['type' => Type::string()],
        ];
    }
}
