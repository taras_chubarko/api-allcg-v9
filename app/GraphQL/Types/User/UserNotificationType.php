<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserNotificationType extends GraphQLType
{
    const NAME = 'UserNotificationType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'activity_comment' => ['type' => Type::boolean()],
            'activity_content' => ['type' => Type::boolean()],
            'activity_register' => ['type' => Type::boolean()],
            'friends_content' => ['type' => Type::boolean()],
            'friends_friendship_accepts' => ['type' => Type::boolean()],
            'friends_friendship_request' => ['type' => Type::boolean()],
            'groups_admin' => ['type' => Type::boolean()],
            'groups_closed' => ['type' => Type::boolean()],
            'groups_invite' => ['type' => Type::boolean()],
            'groups_invite_closed' => ['type' => Type::boolean()],
            'groups_update' => ['type' => Type::boolean()],
            'message_new' => ['type' => Type::boolean()],
            'shop_comment' => ['type' => Type::boolean()],
            'shop_referal' => ['type' => Type::boolean()],
            'groups_invite_private' => ['type' => Type::boolean()],
        ];
    }
}
