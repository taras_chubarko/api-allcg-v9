<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use App\GraphQL\Types\Other\LabelValueIntType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserEditType extends GraphQLType
{
    const NAME = 'UserEditType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'roles' => ['type' => Type::nonNull(Type::listOf(Type::string()))],
            'software' => ['type' => Type::listOf(Type::string())],
            'specialization' => ['type' => Type::listOf(Type::string())],
            'experience' => ['type' => Type::int()],
            'gender' => ['type' => Type::string()],
            'full_name' => ['type' => Type::string()],
            'first_name' => ['type' => Type::string()],
            'last_name' => ['type' => Type::string()],
            'birthday' => ['type' => Type::string()],
            'login' => ['type' => Type::string()],
            'address' => ['type' => GraphQL::type(LabelValueIntType::NAME)],
        ];
    }
}
