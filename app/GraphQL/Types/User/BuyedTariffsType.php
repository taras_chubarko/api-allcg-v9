<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class BuyedTariffsType extends GraphQLType
{
    const NAME = 'BuyedTariffs';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'category' => ['type' => Type::string()],
            'free_download' => ['type' => Type::int()],
            'total_buy' => ['type' => Type::int()],
            'status' => ['type' => Type::int()],
        ];
    }
}
