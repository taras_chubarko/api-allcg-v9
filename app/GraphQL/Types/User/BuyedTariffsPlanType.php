<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class BuyedTariffsPlanType extends GraphQLType
{
    const NAME = 'BuyedTariffsPlanType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'amount' => ['type' => Type::int()],
            'days' => ['type' => Type::int()],
            'products' => ['type' => Type::int()],
        ];
    }
}
