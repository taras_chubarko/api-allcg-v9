<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use App\GraphQL\Types\Other\LabelValueIntType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    const NAME = 'UserType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())],
            'email' => ['type' => Type::nonNull(Type::string())],
            'is_activate' => ['type' => Type::nonNull(Type::boolean())],
            'roles' => ['type' => Type::nonNull(Type::listOf(Type::string()))],
            'gender' => ['type' => Type::string()],
            'specialization' => ['type' => Type::listOf(Type::string())],
            'programs' => ['type' => Type::listOf(Type::string())],
            'full_name' => ['type' => Type::string()],
            'first_name' => ['type' => Type::string()],
            'last_name' => ['type' => Type::string()],
            'avatar' => ['type' => Type::string()],
            'bg' => ['type' => Type::string()],
            'software' => ['type' => Type::listOf(Type::string())],
            'activity' => ['type' => Type::listOf(Type::string())],
            'experience' => ['type' => Type::int()],
            'birthday' => ['type' => Type::string()],
            'address' => ['type' => GraphQL::type(LabelValueIntType::NAME)],
            'login' => ['type' => Type::string()],
            'created_at' => ['type' => Type::string()],
            'is_online' => ['type' => Type::boolean()],
            'zip' => ['type' => Type::string()],
            'pixBalance' => ['type' => Type::int()],
            'rating' => ['type' => GraphQL::type(UserRatingType::NAME)],
            'created' => ['type' => Type::string()],
            'last_products_and_gallery' => ['type' => Type::listOf(Type::string())],
        ];
    }
}
