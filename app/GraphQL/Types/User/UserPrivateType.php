<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserPrivateType extends GraphQLType
{
    const NAME = 'UserPrivateType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'inn' => ['type' => Type::string()],
            'scan_inn' => ['type' => Type::string()],
            'scan_passport' => ['type' => Type::string()],
            'scan_inn_verify_at' => ['type' => Type::string()],
            'scan_passport_at' => ['type' => Type::string()],
        ];
    }
}
