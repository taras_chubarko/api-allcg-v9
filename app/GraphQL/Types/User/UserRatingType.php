<?php

declare(strict_types=1);

namespace App\GraphQL\Types\User;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserRatingType extends GraphQLType
{
    const NAME = 'UserRatingType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'rating' => ['type' => Type::int()],
            'followers' => ['type' => Type::int()],
            'views' => ['type' => Type::int()],
            'likes' => ['type' => Type::int()],
            'count_products' => ['type' => Type::int()],
        ];
    }
}
