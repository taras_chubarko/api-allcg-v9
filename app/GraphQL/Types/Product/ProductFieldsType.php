<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use App\Models\CatalogItem;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductFieldsType extends GraphQLType
{
    const NAME = 'ProductFieldsType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'administered' => ['type' => Type::string()],
            'adobe_version' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'adult_content' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'after_effects' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'age' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'alpha_channel' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'animals' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'animation' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'bit_rate' => ['type' => Type::string()],
            'collection' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'colour' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'columns' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'compatible_browsers' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'compatible_with' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'composer' => ['type' => Type::string()],
            'composition' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'demo_url' => ['type' => Type::string()],
            'drawings' => ['type' => Type::boolean()],
            'dscan' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'effect' => ['type' => Type::int()],
            'emotion' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'environment' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'ethnicity' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'files_included' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'for_printing' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'frame_rate' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'gender' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'geometry' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'high_resolution' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'included' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'layered' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'layout' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'lenght' => ['type' => Type::int()],
            'looped_audio' => ['type' => Type::boolean()],
            'looped_video' => ['type' => Type::boolean()],
            'low_poly' => ['type' => Type::boolean()],
            'mapping' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'materials' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'media_placeholders' => ['type' => Type::string()],
            'movement' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'number_clips' => ['type' => Type::int()],
            'orientation' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'output_files' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'pace' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'people' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'pixel_dimensions' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'plugins' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'polygons' => ['type' => Type::int()],
            'print_dimentions' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'printing' => ['type' => GraphQL::type(ProductPrintingType::NAME)],
            'publisher' => ['type' => Type::string()],
            'registered' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'renderer' => ['type' => Type::listOf(\GraphQL::type(ProductsFieldsNameType::NAME))],
            'requires_plugins' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'resolution' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'resolution_height' => ['type' => Type::int()],
            'resolution_width' => ['type' => Type::int()],
            'sample_sate' => ['type' => Type::string()],
            'seamless' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'size' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'skeleton' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'software' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'software_version' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'source_audio' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'tempo' => ['type' => Type::string()],
            'text_placeholders' => ['type' => Type::string()],
            'texture' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'tops' => ['type' => Type::int()],
            'track_lenght' => ['type' => Type::string()],
            'type' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'universal_expressions' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'unwrapped' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'used_plugins' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'video_encoding' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'vocals' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'with_layers' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'style' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
            'official_model' => ['type' => GraphQL::type(ProductsFieldsNameType::NAME)],
        ];
    }

    //
    protected function resolveAdministeredField($root, $args)
    {
        if (!empty($root['administered'])) {
            return $root['administered'];
        } else {
            return null;
        }
    }

//
    protected function resolveAdobeVersionField($root, $args)
    {
        if (!empty($root['adobe_version'])) {
            $item = CatalogItem::find($root['adobe_version']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveAdultContentField($root, $args)
    {
        if (!empty($root['adult_content'])) {
            $item = CatalogItem::find($root['adult_content']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveAfterEffectsField($root, $args)
    {
        if (!empty($root['after_effects'])) {
            return $root['after_effects'];
        } else {
            return null;
        }
    }

//
    protected function resolveAgeField($root, $args)
    {
        if (!empty($root['age'])) {
            $item = CatalogItem::find($root['age']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveAlphaChannelField($root, $args)
    {
        if (!empty($root['alpha_channel'])) {
            $item = CatalogItem::find($root['alpha_channel']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveAnimalsField($root, $args)
    {
        if (!empty($root['animals'])) {
            $item = CatalogItem::find($root['animals']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveAnimationField($root, $args)
    {
        if (!empty($root['animation'])) {
            $item = CatalogItem::find($root['animation']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveBitRateField($root, $args)
    {
        if (!empty($root['bit_rate'])) {
            return $root['bit_rate'];
        } else {
            return null;
        }
    }

//
    protected function resolveCollectionField($root, $args)
    {
        if (!empty($root['collection'])) {
            $item = CatalogItem::find($root['collection']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveColourField($root, $args)
    {
        if (!empty($root['colour'])) {
            $item = CatalogItem::find($root['colour']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveColumnsField($root, $args)
    {
        if (!empty($root['columns'])) {
            $item = CatalogItem::find($root['columns']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveCompatibleBrowsersField($root, $args)
    {
        if (!empty($root['compatible_browsers'])) {
            $item = CatalogItem::find($root['compatible_browsers']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveCompatibleWithField($root, $args)
    {
        if (!empty($root['compatible_with'])) {
            $item = CatalogItem::find($root['compatible_with']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveComposerField($root, $args)
    {
        if (!empty($root['composer'])) {
            return $root['composer'];
        } else {
            return null;
        }
    }

//
    protected function resolveCompositionField($root, $args)
    {
        if (!empty($root['composition'])) {
            $item = CatalogItem::find($root['composition']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveDemoUrlField($root, $args)
    {
        if (!empty($root['demo_url'])) {
            return $root['demo_url'];
        } else {
            return null;
        }
    }

//
    protected function resolveDrawingsField($root, $args)
    {
        if (!empty($root['drawings'])) {
            return $root['drawings'];
        } else {
            return null;
        }
    }

//
    protected function resolveDscanField($root, $args)
    {
        if (!empty($root['dscan'])) {
            $item = CatalogItem::find($root['dscan']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveEffectField($root, $args)
    {
        if (!empty($root['effect'])) {
            $item = CatalogItem::find($root['effect']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveEmotionField($root, $args)
    {
        if (!empty($root['emotion'])) {
            $item = CatalogItem::find($root['emotion']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveEnvironmentField($root, $args)
    {
        if (!empty($root['environment'])) {
            $item = CatalogItem::find($root['environment']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveEthnicityField($root, $args)
    {
        if (!empty($root['ethnicity'])) {
            $item = CatalogItem::find($root['ethnicity']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveFilesIncludedField($root, $args)
    {
        if (!empty($root['files_included'])) {
            $item = CatalogItem::find($root['files_included']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveForPrintingField($root, $args)
    {
        if (!empty($root['for_printing'])) {
            $item = CatalogItem::find($root['for_printing']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveFrameRateField($root, $args)
    {
        if (!empty($root['frame_rate'])) {
            $item = CatalogItem::find($root['frame_rate']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveGenderField($root, $args)
    {
        if (!empty($root['gender'])) {
            $item = CatalogItem::find($root['gender']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveGeometryField($root, $args)
    {
        if (!empty($root['geometry'])) {
            $item = CatalogItem::find($root['geometry']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveHighResolutionField($root, $args)
    {
        if (!empty($root['high_resolution'])) {
            $item = CatalogItem::find($root['high_resolution']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveIncludedField($root, $args)
    {
        if (!empty($root['included'])) {
            $item = CatalogItem::find($root['included']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveLayeredField($root, $args)
    {
        if (!empty($root['layered'])) {
            $item = CatalogItem::find($root['layered']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveLayoutField($root, $args)
    {
        if (!empty($root['layout'])) {
            $item = CatalogItem::find($root['layout']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveLenghtField($root, $args)
    {
        if (!empty($root['lenght'])) {
            return $root['lenght'];
        } else {
            return null;
        }
    }

//
    protected function resolveLoopedAudioField($root, $args)
    {
        if (!empty($root['looped_audio'])) {
            return $root['looped_audio'];
        } else {
            return null;
        }
    }

//
    protected function resolveLoopedVideoField($root, $args)
    {
        if (!empty($root['looped_video'])) {
            return $root['looped_video'];
        } else {
            return null;
        }
    }

//
    protected function resolveLowPolyField($root, $args)
    {
        if (!empty($root['low_poly'])) {
            return $root['low_poly'];
        } else {
            return null;
        }
    }

//
    protected function resolveMappingField($root, $args)
    {
        if (!empty($root['mapping'])) {
            $item = CatalogItem::find($root['mapping']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveMaterialsField($root, $args)
    {
        if (!empty($root['materials'])) {
            $item = CatalogItem::find($root['materials']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveMediaPlaceholdersField($root, $args)
    {
        if (!empty($root['media_placeholders'])) {
            return $root['media_placeholders'];
        } else {
            return null;
        }
    }

//
    protected function resolveMovementField($root, $args)
    {
        if (!empty($root['movement'])) {
            $item = CatalogItem::find($root['movement']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveNumberClipsField($root, $args)
    {
        if (!empty($root['number_clips'])) {
            return $root['number_clips'];
        } else {
            return null;
        }
    }

//
    protected function resolveOrientationField($root, $args)
    {
        if (!empty($root['orientation'])) {
            $item = CatalogItem::find($root['orientation']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveOutputFilesField($root, $args)
    {
        if (!empty($root['output_files'])) {
            $item = CatalogItem::find($root['output_files']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolvePaceField($root, $args)
    {
        if (!empty($root['pace'])) {
            $item = CatalogItem::find($root['pace']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolvePeopleField($root, $args)
    {
        if (!empty($root['people'])) {
            $item = CatalogItem::find($root['people']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolvePixelDimensionsField($root, $args)
    {
        if (!empty($root['pixel_dimensions'])) {
            $item = CatalogItem::find($root['pixel_dimensions']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolvePluginsField($root, $args)
    {
        if (!empty($root['plugins'])) {
            $item = CatalogItem::find($root['plugins']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolvePolygonsField($root, $args)
    {
        if (!empty($root['polygons'])) {
            return $root['polygons'];
        } else {
            return null;
        }
    }

//
    protected function resolvePrintDimentionsField($root, $args)
    {
        if (!empty($root['print_dimentions'])) {
            $item = CatalogItem::find($root['print_dimentions']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolvePrintingField($root, $args)
    {
        if (!empty($root['printing'])) {
            $item = CatalogItem::find($root['printing']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolvePublisherField($root, $args)
    {
        if (!empty($root['publisher'])) {
            return $root['publisher'];
        } else {
            return null;
        }
    }

//
    protected function resolveRegisteredField($root, $args)
    {
        if (!empty($root['registered'])) {
            $item = CatalogItem::find($root['registered']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveRendererField($root, $args)
    {
        if (!empty($root['renderer'])) {
            $item = CatalogItem::find($root['renderer']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveRequiresPluginsField($root, $args)
    {
        if (!empty($root['requires_plugins'])) {
            $item = CatalogItem::find($root['requires_plugins']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveResolutionField($root, $args)
    {
        if (!empty($root['resolution'])) {
            $item = CatalogItem::find($root['resolution']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveResolutionHeightField($root, $args)
    {
        if (!empty($root['resolution_height'])) {
            return $root['resolution_height'];
        } else {
            return null;
        }
    }

//
    protected function resolveResolutionWidthField($root, $args)
    {
        if (!empty($root['resolution_width'])) {
            return $root['resolution_width'];
        } else {
            return null;
        }
    }

//
    protected function resolveSampleSateField($root, $args)
    {
        if (!empty($root['sample_sate'])) {
            return $root['sample_sate'];
        } else {
            return null;
        }
    }

//
    protected function resolveSeamlessField($root, $args)
    {
        if (!empty($root['seamless'])) {
            $item = CatalogItem::find($root['seamless']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveSizeField($root, $args)
    {
        if (!empty($root['size'])) {
            $item = CatalogItem::find($root['size']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveSkeletonField($root, $args)
    {
        if (!empty($root['skeleton'])) {
            $item = CatalogItem::find($root['skeleton']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveSoftwareField($root, $args)
    {
        if (!empty($root['software'])) {
            $item = CatalogItem::find($root['software']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveSoftwareVersionField($root, $args)
    {
        if (!empty($root['software_version'])) {
            $item = CatalogItem::find($root['software_version']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveSourceAudioField($root, $args)
    {
        if (!empty($root['source_audio'])) {
            $item = CatalogItem::find($root['source_audio']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveTempoField($root, $args)
    {
        if (!empty($root['tempo'])) {
            return $root['tempo'];
        } else {
            return null;
        }
    }

//
    protected function resolveTextPlaceholdersField($root, $args)
    {
        if (!empty($root['text_placeholders'])) {
            return $root['text_placeholders'];
        } else {
            return null;
        }
    }

//
    protected function resolveTextureField($root, $args)
    {
        if (!empty($root['texture'])) {
            $item = CatalogItem::find($root['texture']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveTopsField($root, $args)
    {
        if (!empty($root['tops'])) {
            return $root['tops'];
        } else {
            return null;
        }
    }

//
    protected function resolveTrackLenghtField($root, $args)
    {
        if (!empty($root['track_lenght'])) {
            return $root['track_lenght'];
        } else {
            return null;
        }
    }

//
    protected function resolveTypeField($root, $args)
    {
        if (!empty($root['type'])) {
            $item = CatalogItem::find($root['type']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveUniversalExpressionsField($root, $args)
    {
        if (!empty($root['universal_expressions'])) {
            $item = CatalogItem::find($root['universal_expressions']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveUnwrappedField($root, $args)
    {
        if (!empty($root['unwrapped'])) {
            $item = CatalogItem::find($root['unwrapped']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveUsedPluginsField($root, $args)
    {
        if (!empty($root['used_plugins'])) {
            $item = CatalogItem::find($root['used_plugins']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveVideoEncodingField($root, $args)
    {
        if (!empty($root['video_encoding'])) {
            $item = CatalogItem::find($root['video_encoding']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveVocalsField($root, $args)
    {
        if (!empty($root['vocals'])) {
            $item = CatalogItem::find($root['vocals']);
            return $item;
        } else {
            return null;
        }
    }

//
    protected function resolveWithLayersField($root, $args)
    {
        if (!empty($root['with_layers'])) {
            $item = CatalogItem::find($root['with_layers']);
            return $item;
        } else {
            return null;
        }
    }
    //
    protected function resolveStyleField($root, $args)
    {
        if (!empty($root['style'])) {
            $item = CatalogItem::find($root['style']);
            return $item;
        } else {
            return null;
        }
    }
    //
    protected function resolveOfficialModelField($root, $args)
    {
        if (!empty($root['official_model'])) {
            $item = CatalogItem::find('5caf38d7bd302550067b32d1');
            return $item;
        } else {
            return null;
        }
    }
}
