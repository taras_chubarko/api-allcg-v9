<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductSettingsType extends GraphQLType
{
    const NAME = 'ProductSettingsType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'component' => ['type' => Type::string()],
            'fields' => ['type' => GraphQL::type(ProductFieldsType::NAME)],
        ];
    }
}
