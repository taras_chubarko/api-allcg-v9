<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductTariffPlanType extends GraphQLType
{
    const NAME = 'ProductTariffPlanType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'amount' => ['type' => Type::float()],
            'days' => ['type' => Type::int()],
            'products' => ['type' => Type::int()],
            'productsNoPlan' => ['type' => Type::int()],
        ];
    }
}
