<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use App\Facades\ProductFacade;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductPriceType extends GraphQLType
{
    const NAME = 'ProductPriceType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'vid' => ['type' => Type::string()],
            'category' => ['type' => Type::string()],
            'amount' => ['type' => Type::float()],
            'old_amount' => ['type' => Type::float()],
            'amount_sng' => ['type' => Type::int()],
            'persent' => ['type' => Type::int()],
            'pix' => ['type' => Type::int()],
            'rate' => ['type' => Type::int()],
        ];
    }

    protected function resolveAmountField($root)
    {
        return ProductFacade::priceAmount($root);
    }

    public function resolveOldAmountField($root)
    {
        return ProductFacade::priceOldAmount($root);
    }
}
