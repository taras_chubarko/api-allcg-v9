<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductTariffWorldType extends GraphQLType
{
    const NAME = 'ProductTariffWorldType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'amount' => ['type' => Type::float()],
            'amountInPacket' => ['type' => Type::float()],
            'plan' => ['type' =>GraphQL::type(ProductTariffPlanType::NAME)],
        ];
    }
}
