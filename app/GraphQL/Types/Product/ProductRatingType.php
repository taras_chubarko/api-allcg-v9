<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductRatingType extends GraphQLType
{
    const NAME = 'ProductRatingType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'rating' => ['type' => Type::int()],
            'downloads' => ['type' => Type::int()],
            'views' => ['type' => Type::int()],
            'likes' => ['type' => Type::int()],
            'comments' => ['type' => Type::int()],
        ];
    }
}
