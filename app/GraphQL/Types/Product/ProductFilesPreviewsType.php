<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductFilesPreviewsType extends GraphQLType
{
    const NAME = 'ProductFilesPreviewsType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'name' => ['type' => Type::string()],
            'size' => ['type' => Type::int()],
            'ext' => ['type' => Type::string()],
            'path' => ['type' => Type::string()],
            'preview' => ['type' => Type::string()],
            'type' => ['type' => Type::string()],
        ];
    }
}
