<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use App\GraphQL\Types\Catalog\CatalogItemType;
use App\GraphQL\Types\User\UserType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductType extends GraphQLType
{
    public const NAME = 'ProductType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
            'description' => ['type' => Type::string()],
            'content' => ['type' => Type::string()],
            'slug' => ['type' => Type::string()],
            'preview_image_path' => ['type' => Type::string()],
            'price' => ['type' => GraphQL::type(ProductPriceType::NAME)],
            'tags' => [
                'type' => Type::listOf(Type::string()),
                'args' => [
                    'limit' => [
                        'type' => Type::int(),
                    ],
                ],
            ],
            'rating' => ['type' => GraphQL::type(ProductRatingType::NAME)],
            'slides' => ['type' => Type::listOf(Type::string())],
            'order_id' => ['type' => Type::string()],
            'status' => ['type' => Type::int()],
            'formats' => ['type' => Type::listOf(Type::string())],
            'downloadOrBuy' => ['type' => Type::string()],
            'breadcrumbs' => ['type' => Type::listOf(GraphQL::type(CatalogItemType::NAME))],
            'owner' => ['type' => GraphQL::type(UserType::NAME)],
            'settings' => ['type' => GraphQL::type(ProductSettingsType::NAME)],
            'files' => ['type' => GraphQL::type(ProductFilesType::NAME)],
        ];
    }


}
