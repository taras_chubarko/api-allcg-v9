<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductFilesType extends GraphQLType
{
    const NAME = 'ProductFilesType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'previews' => ['type' => Type::listOf(GraphQL::type(ProductFilesPreviewsType::NAME))],
            'archives' => ['type' => Type::listOf(GraphQL::type(ProductFilesArchiveType::NAME))],
        ];
    }
}
