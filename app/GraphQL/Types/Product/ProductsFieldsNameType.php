<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use App\GraphQL\Types\Catalog\CatalogItemNameType;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductsFieldsNameType extends GraphQLType
{
    const NAME = 'ProductsFieldsNameType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'name' => ['type' => GraphQL::type(CatalogItemNameType::NAME)],
        ];
    }
}
