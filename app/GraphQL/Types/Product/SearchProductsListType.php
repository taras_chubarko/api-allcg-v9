<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class SearchProductsListType extends GraphQLType
{
    const NAME = 'SearchProductsListType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type describing search products list.'
    ];

    public function fields(): array
    {
        return [
            'products' => [
                'type' => Type::listOf(GraphQL::type('ProductType')),
                'args' => [
                    'limit' => [
                        'type' => Type::nonNull(Type::int()),
                        'rules' => ['required'],
                    ],
                    'page' => [
                        'type' => Type::nonNull(Type::int()),
                        'rules' => ['required'],
                    ],
                ],
            ],
            'products_count' => [
                'type' => Type::int(),
            ],
            'products_prices_count' => [
                'type' => Type::listOf(GraphQL::type('ProductsListPricesCountType'))
            ]
        ];
    }

    //
    protected function resolveProductsField($root, $args)
    {
        $collection = collect($root['products']);
        return $collection->forPage($args['page'], $args['limit']);
    }

    protected function resolveProductsPricesCountField($root)
    {
        $collection = collect($root['categoryProducts']);
        $prices = $collection->pluck('price.category');
        $categories = $prices->unique()->values();
        $productsPricesCount = collect([]);
        foreach ($categories as $category) {
            $productsPricesCount->push([
                'category' => $category,
                'count' => $prices->filter(function ($val) use ($category) {
                    return $val === $category;
                })->count(),
            ]);
        }
        return $productsPricesCount;
    }

    protected function resolveProductsCountField($root)
    {
        return count($root['products']);
    }
}
