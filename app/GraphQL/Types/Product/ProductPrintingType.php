<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductPrintingType extends GraphQLType
{
    const NAME = 'ProductPrintingType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'width' => ['type' => Type::int()],
            'height' => ['type' => Type::int()],
            'depth' => ['type' => Type::int()],
        ];
    }
}
