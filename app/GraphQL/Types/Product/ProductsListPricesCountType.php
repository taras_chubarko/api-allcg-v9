<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;

class ProductsListPricesCountType extends GraphQLType
{
    const NAME = 'ProductsListPricesCountType';
    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type for prices count for products list.'
    ];

    public function fields(): array
    {
        return [
            'category' => [
                'type' => Type::string(),
            ],
            'count' => [
                'type' => Type::int(),
            ]
        ];
    }
}
