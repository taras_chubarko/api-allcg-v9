<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Product;

use App\GraphQL\Types\User\BuyedTariffsType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductTariffType extends GraphQLType
{
    const NAME = 'ProductTariffType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'category' => ['type' => Type::string()],
            'pix' => ['type' => Type::int()],
            'rate' => ['type' => Type::int()],
            'discount' => ['type' => Type::int()],
            'free_download' => ['type' => Type::int()],
            'total_buy' => ['type' => Type::int()],
            'amount' => ['type' => Type::int()],
            'notActiveCategory' => ['type' => Type::listOf(Type::string())],
            'activeCategory' => ['type' => Type::listOf(Type::string())],
            'buyed' => ['type' => Type::boolean()],
            'total' => ['type' => GraphQL::type(BuyedTariffsType::NAME)],
        ];
    }
}
