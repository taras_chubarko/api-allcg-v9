<?php

declare(strict_types=1);

namespace App\GraphQL\Types\ProductsFilters;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductsFiltersFieldNameType extends GraphQLType
{
    const NAME = 'ProductsFiltersFieldNameType';

    protected $attributes = [
        'name' => 'ProductsFiltersFieldNameType',
        'description' => 'A type describing products filters field name in a few languages.'
    ];

    public function fields(): array
    {
        return [
            'en' => ['type' => Type::string()],
            'ru' => ['type' => Type::string()],
            'uk' => ['type' =>Type::string()],
        ];
    }
}
