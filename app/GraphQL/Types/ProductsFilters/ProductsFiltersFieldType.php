<?php

declare(strict_types=1);

namespace App\GraphQL\Types\ProductsFilters;

use App\GraphQL\Enums\ProductsFilters\ProductsFiltersFieldTypeEnum;
use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class ProductsFiltersFieldType extends GraphQLType
{
    const NAME = 'ProductsFiltersFieldType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type describing products filters field.'
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'slug' => [
                'type' => Type::nonNull(Type::string())
            ],
            'catalog_item_id' => [
                'type' => Type::string()
            ],
            'type' => [
                'type' => GraphQL::type(ProductsFiltersFieldTypeEnum::NAME),
            ],
            'name' => [
                'type' => GraphQL::type('ProductsFiltersFieldNameType'),
            ],
            'catalogItem' => [
                'type' => GraphQL::type('CatalogItemType')
            ],
            'selectItems' => [
                'type' => Type::listOf(GraphQL::type('CatalogItemType'))
            ]
        ];
    }

    public function resolveSelectItemsField($root)
    {
        return $root->getSelectItems();
    }
}
