<?php

declare(strict_types=1);

namespace App\GraphQL\Types\ProductsFilters;

use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ProductsFiltersType extends GraphQLType
{
    const NAME = 'ProductsFiltersType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Return type for ProductsFiltersQuery.'
    ];

    public function fields(): array
    {
        return [
            'components' => [
                'type' => Type::listOf(GraphQL::type(ProductsFiltersComponentType::NAME)),
            ],
            'fields' => [
                'type' => Type::listOf(GraphQL::type(ProductsFiltersFieldType::NAME)),
            ],
        ];
    }
}
