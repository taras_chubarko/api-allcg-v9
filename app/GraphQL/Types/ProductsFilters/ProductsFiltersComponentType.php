<?php

declare(strict_types=1);

namespace App\GraphQL\Types\ProductsFilters;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use App\GraphQL\Types\Catalog\CatalogItemType;

class ProductsFiltersComponentType extends GraphQLType
{
    const NAME = 'ProductsFiltersComponentType';

    protected $attributes = [
        'name' => self::NAME,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'component' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'fields' => [
                'type' => Type::listOf(GraphQL::type(ProductsFiltersFieldType::NAME)),
            ],
            'categories' => [
                'type' => Type::listOf(GraphQL::type(CatalogItemType::NAME)),
            ],
        ];
    }

    protected function resolveCategoriesField($root)
    {
        return $root->getCatalogCategories();
    }
}
