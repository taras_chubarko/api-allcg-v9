<?php

declare(strict_types=1);

namespace App\GraphQL\Types\ProductsFilters;

use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class CreateProductFiltersComponentAvailableCategoriesType extends GraphQLType
{
    const NAME = 'CreateProductFiltersComponentAvailableCategoriesType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Return type for CreateProductFiltersComponentAvailableCategoriesQuery.'
    ];

    public function fields(): array
    {
        return [
            'components' => [
                'type' => Type::listOf(Type::string()),
            ],
            'categories' => [
                'type' => Type::listOf(GraphQL::type('CatalogItemType')),
            ]
        ];
    }
}
