<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Cart;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CartItemOptionsType extends GraphQLType
{
    const NAME = 'CartItemOptionsType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'category' => ['type' => Type::string()],
            'old_amount' => ['type' => Type::float()],
            'message' => ['type' => Type::string()],
        ];
    }
}
