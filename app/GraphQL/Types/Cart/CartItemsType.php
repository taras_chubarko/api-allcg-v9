<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Cart;

use App\GraphQL\Types\Product\ProductType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CartItemsType extends GraphQLType
{
    const NAME = 'CartItemsType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'id' => ['type' => Type::string()],
            'instance' => ['type' => Type::string()],
            'product_id' => ['type' => Type::string()],
            'name' => ['type' => Type::string()],
            'qty' => ['type' => Type::int()],
            'price' => ['type' => Type::float()],
            'options' => ['type' => GraphQL::type(CartItemOptionsType::NAME)],
            'product' => ['type' => GraphQL::type(ProductType::NAME)]
        ];
    }
}
