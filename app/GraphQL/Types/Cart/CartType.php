<?php

declare(strict_types=1);

namespace App\GraphQL\Types\Cart;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CartType extends GraphQLType
{
    const NAME = 'CartType';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A type'
    ];

    public function fields(): array
    {
        return [
            'count' => ['type' => Type::int()],
            'total' => ['type' => Type::float()],
            'items' => ['type' => Type::listOf(GraphQL::type(CartItemsType::NAME))],
        ];
    }
}
