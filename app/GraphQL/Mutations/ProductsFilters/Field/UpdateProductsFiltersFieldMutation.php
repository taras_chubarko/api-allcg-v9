<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\ProductsFilters\Field;

use App\GraphQL\Enums\ProductsFilters\ProductsFiltersFieldTypeEnum;
use App\Models\ProductsFiltersField;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class UpdateProductsFiltersFieldMutation extends Mutation
{
    const NAME = 'UpdateProductsFiltersFieldMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A mutation to update products filters field attributes.'
    ];

    public function type(): Type
    {
        return GraphQL::type('ProductsFiltersFieldType');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string()
            ],
            'slug' => [
                'type' => Type::string()
            ],
            'catalog_item_id' => [
                'type' => Type::string()
            ],
            'name' => [
                'type' => GraphQL::type('ProductsFiltersFieldNameInput')
            ],
            'type' => [
                'type' => GraphQL::type('ProductsFiltersFieldTypeEnum')
            ],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'id' => ['required', 'exists:products_filters_fields,_id'],
            'slug' => [
                Rule::unique('products_filters_fields', 'slug')->ignore($args['id'], '_id')
            ],
            'catalog_item_id' => [
                'nullable',
                'exists:catalog_items,_id',
                Rule::requiredIf(function () use ($args) {
                    $type = $args['type'] ?? null;
                    return
                        $type && (
                            $type === ProductsFiltersFieldTypeEnum::VALUES['SELECT'] ||
                            $type === ProductsFiltersFieldTypeEnum::VALUES['MULTIPLE_SELECT']
                        );
                })
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $type = $args['type'] ?? null;
        $name = $args['name'] ?? null;
        if ($type && $type === ProductsFiltersFieldTypeEnum::VALUES['CHECKBOX']) {
            $args['catalog_item_id'] = null;
        }

        $fieldToUpdate = ProductsFiltersField::query()->where('_id', $args['id'])->first();
        $fieldToUpdate->update([
            ...$args,
            'name' => [...$fieldToUpdate->name, ...$name]
        ]);
        return $fieldToUpdate;
    }
}
