<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\ProductsFilters\Field;

use App\GraphQL\Enums\ProductsFilters\ProductsFiltersFieldTypeEnum;
use App\GraphQL\Inputs\ProductsFilters\ProductsFiltersFieldNameInput;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersFieldType;
use App\Models\ProductsFiltersField;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\Facades\GraphQL;

class CreateProductsFiltersFieldMutation extends Mutation
{
    const NAME = 'CreateProductsFiltersFieldMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A mutation to create products filters field.'
    ];

    public function type(): Type
    {
        return GraphQL::type(ProductsFiltersFieldType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => [
                'type' => Type::string()
            ],
            'catalog_item_id' => [
                'type' => Type::string(),
            ],
            'name' => [
                'type' => GraphQL::type(ProductsFiltersFieldNameInput::NAME),
            ],
            'type' => [
                'type' => GraphQL::type(ProductsFiltersFieldTypeEnum::NAME)
            ],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'name' => ['required'],
            'slug' => ['required', 'unique:products_filters_fields,slug'],
            'type' => ['required'],
            'catalog_item_id' => [
                'nullable',
                'exists:catalog_items,_id',
                'required_if:type,select,multipleSelect',
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if ($args['type'] === ProductsFiltersFieldTypeEnum::VALUES['CHECKBOX']) {
            unset($args['catalog_item_id']);
        }
        return ProductsFiltersField::create($args);
    }
}
