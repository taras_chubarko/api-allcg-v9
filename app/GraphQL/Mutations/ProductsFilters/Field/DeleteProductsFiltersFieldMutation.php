<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\ProductsFilters\Field;

use App\Models\ProductsFiltersField;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class DeleteProductsFiltersFieldMutation extends Mutation
{
    const NAME = 'DeleteProductsFiltersFieldMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A mutation to completely delete products filters field from database.'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'rules' => ['required', 'exists:products_filters_fields,_id'],
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $id = $args['id'];
        $productsFieldToDelete = ProductsFiltersField::query()->where('_id', $id)->first();

        $productsFieldToDelete->components()->detach();
        $productsFieldToDelete->delete();
        return $args['id'];
    }
}
