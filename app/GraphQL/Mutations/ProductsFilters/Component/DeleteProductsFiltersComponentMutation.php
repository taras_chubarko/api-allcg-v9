<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\ProductsFilters\Component;

use App\Models\ProductComponent;
use App\Models\ProductsFiltersComponent;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class DeleteProductsFiltersComponentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'DeleteProductsFiltersComponentMutation',
        'description' => 'A mutation to delete filters components.'
    ];

    public function type(): Type
    {
        return Type::listOf(Type::string());
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'rules' => ['required', 'exists:products_filters_components,_id']
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $id = $args['id'];
        $productsFiltersComponentToDelete = ProductsFiltersComponent::where('_id', $id)->first();

        $productsFiltersComponentToDelete->fields()->detach();
        ProductComponent::where('component', $productsFiltersComponentToDelete->component)->delete();

        $productsFiltersComponentToDelete->delete();
        return $args['id'];
    }
}
