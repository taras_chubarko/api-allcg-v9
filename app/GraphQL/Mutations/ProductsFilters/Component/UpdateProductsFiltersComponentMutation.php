<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\ProductsFilters\Component;

use App\Models\CatalogItem;
use App\Models\ProductComponent;
use App\Models\ProductsFiltersComponent;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class UpdateProductsFiltersComponentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'UpdateProductsFiltersComponentMutation',
        'description' => 'A mutation to update products filters component.'
    ];

    public function type(): Type
    {
        return GraphQL::type('ProductsFiltersComponentType');
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
            'component' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'categories_ids' => [
                'type' => Type::listOf(Type::string()),
                'defaultValue' => [],
            ],
            'fields_ids' => [
                'type' => Type::listOf(Type::string()),
                'defaultValue' => [],
            ],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'id' => ['required', 'exists:products_filters_components,_id'],
            'component' => [
                'required',
                Rule::when(function () use ($args) {
                    $productsFiltersComponent = ProductsFiltersComponent::all()->firstWhere('_id', $args['id']);
                    return $productsFiltersComponent->component !== $args['component'];
                }, 'unique:products_filters_components,component'),
            ],
            'fields_ids' => ['array'],
            'fields_ids.*' => [
                'exists:products_filters_fields,_id'
            ],
            'categories_ids' => ['array'],
            'categories_ids.*' => [
                'exists:catalog_items,_id'
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $id = $args['id'];
        $component = $args['component'];
        $categoriesIds = $args['categories_ids'];
        $fieldsIds = $args['fields_ids'];

        $productsFiltersComponentToUpdate = ProductsFiltersComponent::where('_id', $id)->first();
        $categories = CatalogItem::whereIn('_id', $categoriesIds)->get();

        $prevComponent = $productsFiltersComponentToUpdate->component;
        $componentFields = $productsFiltersComponentToUpdate->fields();
        $productComponents = $productsFiltersComponentToUpdate->productComponents();

        $componentFields->detach();
        $componentFields->attach($fieldsIds);

        $categories->each(function ($item) use ($component, $prevComponent, $productComponents) {
            if ($component !== $prevComponent && $productComponents->where('category_id', $item->id)->first()) {
                ProductComponent::where('category_id', $item->id)->update([
                    'component' => $component
                ]);
            } else {
                ProductComponent::create([
                    'category_id' => $item->id,
                    'component' => $component
                ]);
            }
        });
        $productComponents->whereNotIn('category_id', $categoriesIds)->delete();

        $updatedComponent = ['component' => $component, 'fields_ids' => $fieldsIds];
        $productsFiltersComponentToUpdate->update($updatedComponent);

        return $productsFiltersComponentToUpdate;
    }
}
