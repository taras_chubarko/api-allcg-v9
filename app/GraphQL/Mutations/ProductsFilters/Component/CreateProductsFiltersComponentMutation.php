<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\ProductsFilters\Component;

use App\Models\Catalog;
use App\Models\CatalogItem;
use App\Models\ProductComponent;
use App\Models\ProductsFiltersComponent;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class CreateProductsFiltersComponentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'CreateProductsFiltersComponentMutation',
        'description' => 'A mutation to create products filters components.'
    ];

    public function type(): Type
    {
        return GraphQL::type('ProductsFiltersComponentType');
    }

    public function args(): array
    {
        return [
            'is_existing_component' => [
                'type' => Type::boolean(),
            ],
            'component' => [
                'type' => Type::string(),
                'rules' => ['required']
            ],
            'categories_ids' => [
                'type' => Type::listOf(Type::string()),
                'defaultValue' => [],
            ],
            'fields_ids' => [
                'type' => Type::listOf(Type::string()),
                'defaultValue' => [],
            ],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'is_existing_component' => ['required'],
            'component' => [
                'required',
                'unique:products_filters_components,component',
                Rule::when(function () use ($args) {
                    return isset($args['is_existing_component']) && $args['is_existing_component'];
                }, 'exists:product_category_component,component'),
            ],
            'fields_ids' => ['array'],
            'fields_ids.*' => [
                'exists:products_filters_fields,_id'
            ],
            'categories_ids' => ['array'],
            'categories_ids.*' => [
                'exists:catalog_items,_id'
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        [
            'component' => $component,
            'categories_ids' => $categoriesIds,
            'fields_ids' => $fieldsIds
        ] = $args;

        $productsFiltersComponent = ProductsFiltersComponent::create([
            'component' => $component,
        ]);

        if (count($categoriesIds)) {
            $categoriesCatalogId = Catalog::where('slug', 'kategoriji')->first()->id;
            $categories = CatalogItem::where('catalog_id', $categoriesCatalogId)
                ->get()->whereIn('_id', $categoriesIds);

            $categories->each(function ($item) use ($component) {
                ProductComponent::create([
                    'category_id' => $item->id,
                    'component' => $component
                ]);
            });
        }

        if (count($fieldsIds)) {
            $productsFiltersComponent->fields()->attach($fieldsIds);
        }

        return $productsFiltersComponent;
    }
}
