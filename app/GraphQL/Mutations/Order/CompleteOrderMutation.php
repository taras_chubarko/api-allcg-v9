<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Order;

use App\Facades\OrderFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CompleteOrderMutation extends Mutation
{
    const NAME = 'CompleteOrderMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Завершення замовлення'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'order' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $data = json_decode($args['order'], true);
        OrderFacade::complete($data);
        return true;
    }
}
