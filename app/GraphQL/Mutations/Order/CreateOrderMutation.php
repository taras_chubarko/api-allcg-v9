<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Order;

use App\Facades\OrderFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CreateOrderMutation extends Mutation
{
    const NAME = 'CreateOrderMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Створення замовлення'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'amount' => ['type' => Type::nonNull(Type::float())],
            'email' => ['type' => Type::nonNull(Type::string())],
            'items' => ['type' => Type::nonNull(Type::string())],
            'ip' => ['type' => Type::nonNull(Type::string())],
            'card_id' => ['type' => Type::string()],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'amount' => ['required'],
            'email' => ['required', 'email'],
            'items' => ['required', 'string'],
            'ip' => ['required', 'string'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return OrderFacade::make($args);
    }
}
