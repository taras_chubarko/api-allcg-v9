<?php

namespace App\GraphQL\Mutations\User\Profile\Chat;

use App\GraphQL\Enums\Chat\ChatTypeEnum;
use App\Models\User;
use App\Models\User\Profile\Chat\Chat;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Validation\Rule;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class CreateChatMutation extends Mutation
{
    protected $attributes = [
        'name' => 'CreateChatMutation',
        'description' => 'A mutation to create new user chats.'
    ];

    public function type(): Type
    {
        return GraphQL::type('ChatType');
    }

    public function args(): array
    {
        return [
            'type' => [
                'type' => GraphQL::type(ChatTypeEnum::NAME)
            ],
            'user_ids' => [
                'type' => Type::listOf(Type::string()),
            ],
            'name' => [
                'type' => Type::string()
            ],
            'nickname' => [
                'type' => Type::string()
            ],
            'avatar' => [
                'type' => GraphQL::type('Upload'),
            ],
        ];
    }

    protected function rules(array $args = []): array
    {
        $requiredIfNotDialogRule = 'required_if:type,' . ChatTypeEnum::VALUES['GROUP'] . ',' . ChatTypeEnum::VALUES['CHANNEL'];
        return [
            'name' => ['nullable', $requiredIfNotDialogRule],
            'type' => ['required'],
            'nickname' => ['nullable', 'unique:chats,nickname'],
            'avatar' => ['nullable'],
            'user_ids' => ['array', 'min:1', Rule::when(function () use ($args) {
                return $args['type'] === ChatTypeEnum::VALUES['DIALOG'];
            }, 'max:1')],
            'user_ids.*' => ['exists:users,_id']
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $chat = Chat::create($args);
        $user = auth()->user();

        $chat->members()->attach([...$args['user_ids'], $user->id]);

        if ($chat->type !== ChatTypeEnum::VALUES['DIALOG']) {
            $chat->creator_id = $user->id;
            $chat->save();
        }

        $users = User::whereIn('_id', $args['user_ids'])->get();
        collect([...$users, $user])->each(function ($user) use ($chat) {
            $user->chats()->attach($chat->id);
        });

        return $chat;
    }
}
