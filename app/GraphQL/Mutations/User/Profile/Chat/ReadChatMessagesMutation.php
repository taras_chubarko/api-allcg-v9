<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User\Profile\Chat;

use App\Events\User\Profile\Chat\ChatMessagesReadEvent;
use App\Models\User\Profile\Chat\Chat;
use App\Models\User\Profile\Chat\ChatMessage;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class ReadChatMessagesMutation extends Mutation
{
    protected $attributes = [
        'name' => 'ReadChatMessagesMutation',
        'description' => 'A mutation to mark chat messages as read and send read messages event.'
    ];

    // Return array of message ids where read status was changed to "read"
    public function type(): Type
    {
        return Type::listOf(Type::string());
    }

    public function args(): array
    {
        return [
            'chat_id' => [
                'type' => Type::string(),
            ],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'chat_id' => [
                'exists:chats,_id',
                function ($attribute, $value, $fail) {
                    $user = auth()->user();
                    $chat = Chat::find($value);

                    if (!$chat->members->find($user->id)) {
                        $fail("You don't have an access to this chat.");
                    }
                }
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        ['chat_id' => $chatId] = $args;
        $chatMessages = ChatMessage::where('chat_id', $chatId)->latest()->get();

        if ($chatMessages->count() === 0) {
            return [];
        }

        $unreadChatMessages = $chatMessages
            ->where('sender_id', '!==', $user->id)
            ->where('status', 'sent');

        if (count($unreadChatMessages)) {
            broadcast(new ChatMessagesReadEvent($chatId))->toOthers();
            $unreadChatMessages->each(function ($message) {
                $message->markAsRead();
            });
        }
        return $unreadChatMessages->map(function ($message) {
            return $message->id;
        });
    }
}
