<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User\Profile\Chat;

use App\Events\User\Profile\Chat\NewChatMessageEvent;
use App\Models\User\Profile\Chat\Chat;
use App\Models\User\Profile\Chat\ChatMessage;
use App\Models\User\Profile\Chat\ChatMessageAttachment;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Mimey\MimeTypes;
use Illuminate\Support\Facades\Storage;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;

class SendChatMessageMutation extends Mutation
{
    protected $attributes = [
        'name' => 'SendChatMessageMutation',
        'description' => 'A mutation to send message to the chat.'
    ];

    public function type(): Type
    {
        return GraphQL::type('ChatMessageType');
    }

    public function args(): array
    {
        return [
            'text' => [
                'type' => Type::string(),
            ],
            'chat_id' => [
                'type' => Type::string(),
            ],
            'attachments' => [
                'type' => Type::listOf(GraphQL::type('Upload')),
            ]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'text' => ['max:3000', function ($attribute, $value, $fail) use ($args) {
                if ((is_null($args['attachments']) || count($args['attachments']) === 0) && empty($value)) {
                    $fail("Text field must not be empty if there is no attachments");
                }
                return true;
            }],
            'chat_id' => [
                'exists:chats,_id',
                function ($attribute, $value, $fail) {
                    $user = auth()->user();
                    $chat = Chat::find($value);

                    if (!$chat->members->find($user->id)) {
                        $fail("You don't have an access to this chat.");
                    }
                }
            ],
            'attachments' => [
                'nullable',
                'array',
            ],
            'attachments.*' => [
                'file',
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $message = ChatMessage::create([
            'text' => trim($args['text']),
            'chat_id' => $args['chat_id'],
            'sender_id' => auth()->user()->id,
        ]);

        $attachments = $args['attachments'] ?? null;
        if ($attachments && count($attachments)) {
            foreach ($attachments as $attachment) {
                $fileName = $attachment->getClientOriginalName();
                $fileSize = $attachment->getSize();
                $fileExtension = pathinfo($fileName, PATHINFO_EXTENSION);
                $mimeTypes = new MimeTypes();
                $fileType = $mimeTypes->getMimeType($fileExtension);

                $chatMessageAttachment = ChatMessageAttachment::create([
                    'name' => $fileName,
                    'extension' => $fileExtension,
                    'size' => $fileSize,
                    'type' => $fileType,
                    'chat_message_id' => $message->id,
                ]);
                $chatMessageAttachment->path = Storage::putFileAs(
                    'chats/' . $message->chat_id . '/attachments/' . $message->id,
                    $attachment,
                    $chatMessageAttachment->id . '-' . date("Y-m-d_H-i-s") . '.' . $fileExtension
                );
                $chatMessageAttachment->save();
            }
        }
        broadcast(new NewChatMessageEvent($message))->toOthers();

        return $message;
    }
}
