<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Facades\UserFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CardVerifySaveMutation extends Mutation
{
    const NAME = 'CardVerifySaveMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Додавання карти користувача'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'data' => ['type' => Type::string()]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::saveCard(auth()->user(), $args);
    }
}
