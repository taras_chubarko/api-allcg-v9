<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Facades\UserFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UserDeleteBgMutation extends Mutation
{
    const NAME = 'UserDeleteBgMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Видалення фонового зображення користувача'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::deleteBg(auth()->user());
    }
}
