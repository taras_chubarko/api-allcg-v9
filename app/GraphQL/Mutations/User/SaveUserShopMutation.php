<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Models\UserShop;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class SaveUserShopMutation extends Mutation
{
    const NAME = 'SaveUserShopMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Збереження налаштувань магазину.'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'company_name' => ['type' => Type::string()],
            'vat' => ['type' => Type::string()],
            'edprpou' => ['type' => Type::string()],
            'scan_inn' => ['type' => Type::string()],
            'scan_passport' => ['type' => Type::string()],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'company_name' => ['required'],
            'vat' => ['required', 'numeric'],
            'edprpou' => ['numeric'],
            'scan_inn' => ['required', 'string'],
            'scan_passport' => ['required', 'string'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $data = collect($args)->prepend(auth()->id(), 'user_id')->toArray();
        UserShop::query()->updateOrCreate(['user_id' => auth()->id()], $data);
        return true;
    }
}
