<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\GraphQL\Inputs\User\UserNotificationInput;
use App\Models\UserNotifySettings;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UpdateUserNotificationMutation extends Mutation
{
    const NAME = 'UpdateUserNotificationMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Налаштування сповыщень користувача'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'data' => ['type' => GraphQL::type(UserNotificationInput::NAME)]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        UserNotifySettings::query()->updateOrCreate([
            'user_id' => auth()->id(),
        ],
            [
                'user_id' => auth()->id(),
                'data' => $args['data'],
            ]);
        return true;
    }
}
