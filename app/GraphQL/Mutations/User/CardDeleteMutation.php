<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CardDeleteMutation extends Mutation
{
    const NAME = 'CardDeleteMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Видалення картки'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return auth()->user()->cards()->find($args['id'])->delete() || false;
    }
}
