<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CheckReCaptchaMutation extends Mutation
{
    const NAME = 'CheckReCaptchaMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Перевірка re-captcha'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'token' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $dav = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".config('site.recaptcha_secret')."&response=".$args['token']."&remoteip=".$_SERVER['REMOTE_ADDR']);
        $response = json_decode($dav,true);
        return $response['success'];
    }
}
