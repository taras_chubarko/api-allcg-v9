<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Models\UserSubscribes;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UserAddSubscribersMutation extends Mutation
{
    const NAME = 'UserAddSubscribersMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Підписка на акаунти'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'followers' => [
                'type' => Type::listOf(Type::string())
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        foreach ($args['followers'] as $follower){
            UserSubscribes::query()
                ->updateOrCreate([
                    'user_id' => $user->id,
                    'follower_id' => $follower,
                ],[
                    'user_id' => $user->id,
                    'follower_id' => $follower,
                ]);
        }
        return true;
    }
}
