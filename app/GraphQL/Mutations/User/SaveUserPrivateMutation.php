<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Models\UserPrivate;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class SaveUserPrivateMutation extends Mutation
{
    const NAME = 'SaveUserPrivateMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Збереження даних приватної особи.'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'inn' => ['type' => Type::string()],
            'scan_inn' => ['type' => Type::string()],
            'scan_passport' => ['type' => Type::string()],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'inn' => ['required'],
            'scan_inn' => ['required', 'string'],
            'scan_passport' => ['required', 'string'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $data = collect($args)->prepend(auth()->id(), 'user_id')->toArray();
        UserPrivate::query()->updateOrCreate(['user_id' => auth()->id()], $data);
        return true;
    }
}
