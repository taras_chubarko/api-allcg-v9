<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User\Admin;

use App\Facades\UserFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class AdminAuthMutation extends Mutation
{
    const NAME = 'AdminAuthMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A mutation to authorize admin user.'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
            'password' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::signInAdmin($args);
    }
}
