<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Facades\UserFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class SmsActivateMutation extends Mutation
{
    const NAME = 'SmsActivateMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Відправка коду активації по СМС'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'phone' => ['type' => Type::string()]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'phone' => ['required'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::sendSmsCode($args);
    }
}
