<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Models\User;
use App\Rules\OldPaswordRule;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UserAccessMutation extends Mutation
{
    const NAME = 'UserAccessMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Заміна пароля в налаштуваннях користувача'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'password_old' => ['type' => Type::string()],
            'password' => ['type' => Type::string()],
            'password_confirmation' => ['type' => Type::string()],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'password_old' => ['required', 'min:8', new OldPaswordRule()],
            'password' => ['required', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'min:8'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        auth()->user()->update(['password' => $args['password']]);
        return true;
    }
}
