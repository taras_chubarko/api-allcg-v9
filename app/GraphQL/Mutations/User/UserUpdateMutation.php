<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UserUpdateMutation extends Mutation
{
    const NAME = 'UserUpdateMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Оновлення інформацїї про користувача'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'data' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $data = collect(json_decode($args['data']));
        auth()->user()->update($data->except('id')->toArray());
        return true;
    }
}
