<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CardSetForPayOutMutation extends Mutation
{
    const NAME  = 'CardSetForPayOutMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Встановлює мітку для карти яка буде для виплат'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();

        if($user->cards()->count()){

            foreach ($user->cards as $card){
                $c = $user->cards()->find($card->id);
                $c->payOut = false;
                $c->save();
            }

            $item = $user->cards()->find($args['id']);
            $item->payOut = true;
            $item->save();

            return true;
        }

        return false;
    }
}
