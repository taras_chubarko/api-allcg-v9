<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Facades\UserFacade;
use App\Services\Contracts\UserServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class SignInMutation extends Mutation
{
    const NAME = 'SignInMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Авторизація'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'email' => ['type' => Type::nonNull(Type::string())],
            'password' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'email' => ['required', 'email'],
            'password' => ['required', 'min:8'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields, UserServiceInterface $userService)
    {
        return $userService->signIn($args);
    }
}
