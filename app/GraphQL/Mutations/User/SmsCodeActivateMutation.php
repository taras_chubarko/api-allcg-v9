<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Facades\UserFacade;
use App\Rules\SmsCodeExistsRule;
use App\Rules\SmsCodeRule;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class SmsCodeActivateMutation extends Mutation
{
    const NAME = 'SmsCodeActivateMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Активація користувача через смс'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'phone' => ['type' => Type::string()],
            'code' => ['type' => Type::string()],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'phone' => ['required'],
            //, new SmsCodeExistsRule($args['phone']), new SmsCodeRule($args['phone'])
            'code' => ['required'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::smsCodeActivation($args);
    }
}
