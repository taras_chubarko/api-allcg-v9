<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Services\Contracts\FondyServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Str;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class BuyTariffFromCardMutation extends Mutation
{
    const NAME = 'BuyTariffFromCardMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Купівля тарифу з карточки користувача'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'amount' => ['type' => Type::nonNull(Type::int())],
            'sender_email' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo,
                            Closure $getSelectFields, FondyServiceInterface $fondyService)
    {
        $args['order_id'] = 'tariff-'.Str::uuid();
        $result = $fondyService->create($args);
        return $result['token'];
    }
}
