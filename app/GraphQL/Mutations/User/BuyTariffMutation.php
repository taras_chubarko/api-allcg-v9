<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Services\Contracts\UserServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class BuyTariffMutation extends Mutation
{
    const NAME = 'BuyTariffMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Купівля тарифного плану.'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'data' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, array $args, $context,
                            ResolveInfo $resolveInfo, Closure $getSelectFields, UserServiceInterface $userService)
    {
        return $userService->buyTariff($args);
    }
}
