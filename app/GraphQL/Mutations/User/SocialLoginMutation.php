<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Services\Contracts\UserServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class SocialLoginMutation extends Mutation
{
    const NAME = 'SocialLoginMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Авторизація по соц мережам'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'email' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'email' => ['required', 'email'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields,
        UserServiceInterface $userService
    )
    {
        return $userService->signInSoc($args);
    }
}
