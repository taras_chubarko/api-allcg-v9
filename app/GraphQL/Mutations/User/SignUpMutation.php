<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Facades\UserFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class SignUpMutation extends Mutation
{
    const NAME = 'SignUpMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Реєстрація'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'email' => ['type' => Type::nonNull(Type::string())],
            'password' => ['type' => Type::nonNull(Type::string())],
            'password_confirmation' => ['type' => Type::nonNull(Type::string())],
            'recaptcha' => ['type' => Type::nonNull(Type::boolean())],
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'min:8'],
            'recaptcha' => ['required'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::signUp($args);
    }
}
