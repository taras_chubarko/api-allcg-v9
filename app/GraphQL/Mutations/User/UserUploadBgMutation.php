<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Facades\UserFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\UploadType;

class UserUploadBgMutation extends Mutation
{
    const NAME = 'UserUploadBgMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Завантаження фонового зображення користувача'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'file' => ['type' => GraphQL::type('Upload')]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'file' => ['required', 'image'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::uploadBg(auth()->user(), $args);
    }
}
