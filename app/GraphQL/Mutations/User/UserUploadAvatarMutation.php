<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\User;

use App\Facades\UserFacade;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\UploadType;

class UserUploadAvatarMutation extends Mutation
{
    const NAME = 'UserUploadAvatarMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Завантаження аватарки користувача'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'file' => ['type' => new UploadType('UploadAvatar')]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::uploadAvatar(auth()->user(), $args);
    }
}
