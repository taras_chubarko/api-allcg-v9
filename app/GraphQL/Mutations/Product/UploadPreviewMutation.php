<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Product;

use App\GraphQL\Types\Product\ProductFilesPreviewsType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Storage;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\UploadType;

class UploadPreviewMutation extends Mutation
{
    const NAME = 'UploadPreviewMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A mutation'
    ];

    public function type(): Type
    {
        return GraphQL::type(ProductFilesPreviewsType::NAME);
    }

    public function args(): array
    {
        return [
            'file' => [
                'type' => new UploadType('UploadProductPreview'),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $filePath = Storage::putFile('product/' . auth()->id(), $args['file']);
        $file = pathinfo($filePath);
        $result['name'] = $file['basename'];
        $result['path'] = $filePath;
        $result['size'] = filesize(storage_path('app/'.$filePath));
        $result['type'] = mime_content_type(storage_path('app/'.$filePath));
        return $result;
    }
}
