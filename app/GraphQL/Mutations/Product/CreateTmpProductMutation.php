<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Product;

use App\Models\Product;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class CreateTmpProductMutation extends Mutation
{
    const NAME = 'CreateTmpProductMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A mutation'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $product = Product::query()->updateOrCreate([
            'name' => 'TmpProduct',
            'user_id' => auth()->id(),
        ], ['status' => 0,
            'rate' => 0]);
        return $product->id;
    }
}
