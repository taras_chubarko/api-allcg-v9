<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Product;

use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\UploadType;

class UploadFileMutation extends Mutation
{
    const NAME = 'UploadFileMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A mutation'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'file' => [
                'name' => 'file',
                'type' => new UploadType('UploadProductFile'),
                'rules' => ['required'],
            ],
            'folder' => [
                'name' => 'folder',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $file = $args['file'];
        $folder = $args['folder'];
        $user = auth()->user();
        $filePath = Storage::putFile($folder . '/' . $user->id, new File($file));

//        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
//        if ($ext == 'mp4') {
//            $fileName = pathinfo($filePath, PATHINFO_FILENAME) . '.jpg';
//            $upDir = storage_path('app/' . $folder . '/' . $user->id);
//            \VideoThumbnail::createThumbnail(storage_path('app/' . $filePath), $upDir, $fileName, 10, $width = 1024, $height = 640);
//            $filePath = $folder . '/' . $user->id . '/' . $fileName;
//        }
        return $filePath;
    }
}
