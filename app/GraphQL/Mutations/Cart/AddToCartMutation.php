<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations\Cart;

use App\Services\Contracts\CartServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class AddToCartMutation extends Mutation
{
    const NAME = 'AddToCartMutation';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Додавання товару в кошик.'
    ];

    public function type(): Type
    {
        return Type::boolean();
    }

    public function args(): array
    {
        return [
            'visitor_id' => ['type' => Type::nonNull(Type::string())],
            'product_id' => ['type' => Type::nonNull(Type::string())],
            'amount' => ['type' => Type::float()],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields,
                            CartServiceInterface $cartService)
    {
        return $cartService->addToCart($args);
    }
}
