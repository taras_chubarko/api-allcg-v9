<?php

declare(strict_types=1);

namespace App\GraphQL\Enums\Chat;

use Rebing\GraphQL\Support\EnumType;

class ChatTypeEnum extends EnumType
{
    const NAME = 'ChatTypeEnum';

    public const VALUES =  [
        'DIALOG' => 'dialog',
        'GROUP' => 'group',
        'CHANNEL' => 'channel'
    ];

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An enum describing chat types.',
        'values' => self::VALUES
    ];
}
