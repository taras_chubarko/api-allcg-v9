<?php

declare(strict_types=1);

namespace App\GraphQL\Enums\ProductsFilters;

use Rebing\GraphQL\Support\EnumType;

class ProductsFiltersFieldTypeEnum extends EnumType
{
    const NAME = 'ProductsFiltersFieldTypeEnum';

    public const VALUES =  [
        'SELECT' => 'select',
        'MULTIPLE_SELECT' => 'multipleSelect',
        'CHECKBOX' => 'checkbox'
    ];

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An enum for products filters field type.',
        'values' => self::VALUES
    ];
}
