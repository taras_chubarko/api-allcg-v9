<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Product;

use App\GraphQL\Types\Product\ProductType;
use App\Models\Product;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class LastProductByCategoryIdQuery extends Query
{
    const NAME = 'LastProductByCategoryIdQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Товари в меню'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(ProductType::NAME));
    }

    public function args(): array
    {
        return [
            'category_id' => ['type' => Type::nonNull(Type::string())],
            'take' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return Product::query()->where('status', 1)
            ->whereNotNull('files.previews')
            ->whereIn('category', [$args['category_id']])
            ->orderBy('created_at', 'desc')
            ->take($args['take'])
            ->get();
    }
}
