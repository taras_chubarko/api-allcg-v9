<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Product;

use App\Services\Contracts\ProductServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;
use Illuminate\Support\Facades\Crypt;

class ProductDownloadQuery extends Query
{
    const NAME = 'ProductDownloadQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Скачування купленого товару.'
    ];

    public function type(): Type
    {
        return Type::string();
    }

    public function args(): array
    {
        return [
            'ip' => ['type' => Type::nonNull(Type::string())],
            'product_id' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $args['user_id'] = auth()->id();
        $crypt = Crypt::encrypt($args);
        return config('app.url') . '/download/' . $crypt;
    }
}
