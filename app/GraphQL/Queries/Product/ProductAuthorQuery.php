<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Product;

use App\GraphQL\Types\Product\ProductType;
use App\Models\Product;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProductAuthorQuery extends Query
{
    const NAME = 'ProductAuthorQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Товари автора на сторінці товару'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(ProductType::NAME));
    }

    public function args(): array
    {
        return [
            'slug' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $product = Product::where('slug', $args['slug'])->first();
        $products = Product::where('user_id', $product->user_id)->status()->take(10)->get();
        return $products;
    }
}
