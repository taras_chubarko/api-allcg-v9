<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Product;

use App\GraphQL\Types\Product\ProductType;
use App\Models\Product;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProductComponentQuery extends Query
{
    const NAME = 'ProductComponentQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Сторінка товару'
    ];

    public function type(): Type
    {
        return GraphQL::type(ProductType::NAME);
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $product = Product::query()->where('slug', $args['slug'])->first();
        return $product;
    }
}
