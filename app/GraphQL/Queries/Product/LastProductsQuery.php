<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Product;

use App\GraphQL\Types\Product\ProductType;
use App\Models\Product;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class LastProductsQuery extends Query
{
    public const NAME = 'LastProductsQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Останні товари на головній'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(ProductType::NAME));
    }

    public function args(): array
    {
        return [
            'take' => ['type' => Type::nonNull(Type::int())],
            'page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {

        return Product::query()
            ->where('status', 1)
            ->whereNotNull('files.previews')
            ->orderBy('created_at', 'desc')
            ->paginate($args['take'], ['*'], 'page', $args['page']);
    }
}
