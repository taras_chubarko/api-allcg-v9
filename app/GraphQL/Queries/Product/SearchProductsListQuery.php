<?php

namespace App\GraphQL\Queries\Product;

use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use App\Models\CatalogItem;
use Rebing\GraphQL\Support\SelectFields;
use App\Models\Product;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\Facades\GraphQL;

class SearchProductsListQuery extends Query
{
    const NAME = 'SearchProductsListQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query to get filtered products list.'
    ];

    public function type(): Type
    {
        return GraphQL::type('SearchProductsListType');
    }

    public function args(): array
    {
        return [
            'filter' => [
                'name' => 'filter',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $filter = json_decode($args['filter'], true);
        $products = Product::all();
        $categoryProducts = null;
        if (isset($filter['category'])) {
            $catalogItemId = CatalogItem::where('slug', $filter['category'])->firstOrFail()->id;
            $categoryProducts = $products->filter(function ($value) use ($catalogItemId) {
                $categories = collect($value['category']);
                return $categories->contains($catalogItemId);
            });
            $products = $categoryProducts;
        }

        if (isset($filter['price']) && $filter['price']) {
            $products = $products->where('price.category', $filter['price']);
        }

        if (isset($filter['fields']) && count($filter['fields'])) {
            $fields = collect($filter['fields']);
            $fields->each(function ($item, $key) use (&$products) {
                if (is_array($item)) {
                    foreach($item as $field) {
                        $products = $products->filter(function($product) use ($field, $key) {
                            return collect($product["settings.fields.$key"])->contains($field);
                        });
//                        $products = $products->whereIn("settings.fields.$key", $field);
                    }
                } else {
                    $products = $products->where("settings.fields.$key", $item);
                }
            });
        }

        $products = $products->sortByDesc('created_at');
        return collect([
            'products' => $products,
            'categoryProducts' => $categoryProducts,
        ]);
    }
}
