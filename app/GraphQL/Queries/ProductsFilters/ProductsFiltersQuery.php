<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\ProductsFilters;

use App\Models\ProductsFiltersComponent;
use App\Models\ProductsFiltersField;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\Facades\GraphQL;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersType;

class ProductsFiltersQuery extends Query
{
    const NAME = 'ProductsFiltersQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query get all products filters components and fields.'
    ];

    public function type(): Type
    {
        return GraphQL::type(ProductsFiltersType::NAME);
    }

    public function resolve($root, $args)
    {
        $components = ProductsFiltersComponent::all()->sortByDesc('created_at');
        $fields = ProductsFiltersField::all()->sortByDesc('created_at');
        return collect([
            'components' => $components,
            'fields' => $fields,
        ]);
    }
}
