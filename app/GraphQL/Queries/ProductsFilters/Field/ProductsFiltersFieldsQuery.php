<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\ProductsFilters\Field;

use App\Models\ProductsFiltersField;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class ProductsFiltersFieldsQuery extends Query
{
    const NAME = 'ProductsFiltersFieldsQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query to get products filters fields.'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('ProductsFiltersFieldType'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
            ],
            'slug' => [
                'type' => Type::string(),
            ],
            'type' => [
                'type' => GraphQL::type('ProductsFiltersFieldTypeEnum'),
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $result = ProductsFiltersField::all();
        if (isset($args['id'])) {
            $result = $result->where('_id' , $args['id']);
        }

        if (isset($args['slug'])) {
            $result = $result->where('slug', $args['slug']);
        }

        if (isset($args['type'])) {
            $result = $result->where('type', $args['type']);
        }

        return $result;
    }
}
