<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\ProductsFilters\Component;

use App\Models\ProductsFiltersComponent;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class ProductsFiltersComponentsQuery extends Query
{
    protected $attributes = [
        'name' => 'ProductsFiltersComponentsQuery',
        'description' => 'A query to get products filters components.'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('ProductsFiltersComponentType'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
            ],
            'component' => [
                'type' => Type::string(),
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $result = ProductsFiltersComponent::all();

        if (isset($args['id'])) {
            $result = $result->where('_id' , $args['id']);
        }

        if (isset($args['component'])) {
            $result = $result->where('component', $args['component']);
        }

        return $result;
    }
}
