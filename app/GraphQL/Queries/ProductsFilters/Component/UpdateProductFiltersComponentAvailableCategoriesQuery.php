<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\ProductsFilters\Component;

use App\Models\Catalog;
use App\Models\CatalogItem;
use App\Models\ProductComponent;
use App\Models\ProductsFiltersComponent;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\Facades\GraphQL;

class UpdateProductFiltersComponentAvailableCategoriesQuery extends Query
{
    const NAME = 'UpdateProductFiltersComponentAvailableCategoriesQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query to get all available categories for component which have to be updated.'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('CatalogItemType'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::string()),
            ]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'id' => [
                'required',
                'exists:products_filters_components,_id',
            ]
        ];
    }

    public function resolve($root, $args)
    {
        $id = $args['id'];
        $filterComponent = ProductsFiltersComponent::all()->firstWhere('_id', $id);

        $categoriesCatalogId = Catalog::where('slug', 'kategoriji')->first()->id;
        $categories = CatalogItem::where('catalog_id', $categoriesCatalogId)->get();
        $categoriesWithComponentIds = ProductComponent::all()->pluck('category_id');
        $categoriesWithoutComponent = $categories->whereNotIn('_id', $categoriesWithComponentIds);

        return $categoriesWithoutComponent->merge($filterComponent->getCatalogCategories());
    }
}
