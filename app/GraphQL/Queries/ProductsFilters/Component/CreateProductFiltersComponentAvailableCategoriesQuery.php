<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\ProductsFilters\Component;

use App\GraphQL\Types\ProductsFilters\CreateProductFiltersComponentAvailableCategoriesType;
use App\Models\Catalog;
use App\Models\CatalogItem;
use App\Models\ProductComponent;
use App\Models\ProductsFiltersComponent;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\Facades\GraphQL;

class CreateProductFiltersComponentAvailableCategoriesQuery extends Query
{
    const NAME = 'CreateProductFiltersComponentAvailableCategoriesQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query to get all available categories for component which have to be created.'
    ];

    public function type(): Type
    {
        return GraphQL::type(CreateProductFiltersComponentAvailableCategoriesType::NAME);
    }

    public function resolve($root, $args)
    {
        $filtersComponents = ProductsFiltersComponent::all()->pluck('component')->toArray();
        $components = ProductComponent::all()->whereNotIn('component', $filtersComponents)->pluck('component')->unique();
        $categoriesCatalogId = Catalog::where('slug', 'kategoriji')->first()->id;
        $categories = CatalogItem::where('catalog_id', $categoriesCatalogId)->get();
        $categoriesWithComponentIds = ProductComponent::all()->pluck('category_id');
        $categoriesWithoutComponent = $categories->whereNotIn('_id', $categoriesWithComponentIds);

        return [
            'components' => $components,
            'categories' => $categoriesWithoutComponent
        ];
    }
}
