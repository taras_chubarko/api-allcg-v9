<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\ProductsFilters\Component;

use App\Models\CatalogItem;
use App\Models\ProductComponent;
use App\Models\ProductsFiltersComponent;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class CategoryProductsFiltersComponentQuery extends Query
{
    const NAME = 'CategoryProductsFiltersComponentQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query to get component connected to the category.'
    ];

    public function type(): Type
    {
        return GraphQL::type('ProductsFiltersComponentType');
    }

    public function args(): array
    {
        return [
            'category' => ['type' => Type::string()]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'category' => [
                'required',
                'exists:catalog_items,slug',
            ]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $catalogItem = CatalogItem::where('slug', $args['category'])->first();
        $productComponent = ProductComponent::where('category_id', $catalogItem->id)->first();
        if (!$productComponent) {
            return null;
        }
        return ProductsFiltersComponent::where('component', $productComponent->component)->first();
    }
}
