<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Cart;

use App\GraphQL\Types\Cart\CartType;
use App\Services\Contracts\CartServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CartQuery extends Query
{
    const NAME = 'CartQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Кошик'
    ];

    public function type(): Type
    {
        return GraphQL::type(CartType::NAME);
    }

    public function args(): array
    {
        return [
            'visitor_id' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields,
        CartServiceInterface $cartService
    )
    {
        return $cartService->cart($args);
    }
}
