<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\User\UserShopType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class EditUserShopQuery extends Query
{
    const NAME = 'EditUserShopQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Редагування магазину.'
    ];

    public function type(): Type
    {
        return GraphQL::type(UserShopType::NAME);
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return auth()->user()->shop;
    }
}
