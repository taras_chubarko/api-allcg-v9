<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\Product\ProductTariffType;
use App\Services\Contracts\UserServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProfileTariffsQuery extends Query
{
    const NAME = 'ProfileTariffsQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Тарифні плани клристувача.'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(ProductTariffType::NAME));
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields,
                            UserServiceInterface $userService)
    {
       return $userService->tariffs(auth()->user());
    }
}
