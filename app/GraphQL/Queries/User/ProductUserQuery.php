<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\Product\ProductType;
use App\Services\Contracts\UserServiceInterface;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProductUserQuery extends Query
{
    const NAME = 'ProductUserQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(ProductType::NAME);
    }

    public function args(): array
    {
        return [
            'limit' => ['type' => Type::nonNull(Type::int())],
            'page' => ['type' => Type::nonNull(Type::int())],
            'orderBy' => ['type' => Type::string()],
            'folderName' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields, UserServiceInterface $userService)
    {
        return $userService->userProducts(auth()->user(), $args);
    }
}
