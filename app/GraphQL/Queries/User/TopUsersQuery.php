<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\User\UserType;
use App\Models\User;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class TopUsersQuery extends Query
{
    const NAME = 'TopUsersQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Топ користувачів на головній'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(UserType::NAME));
    }

    public function args(): array
    {
        return [
            'take' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return User::query()
            ->where('is_activate', true)
            ->orderByDesc('rate')
            ->take($args['take'])
            ->get();
    }
}
