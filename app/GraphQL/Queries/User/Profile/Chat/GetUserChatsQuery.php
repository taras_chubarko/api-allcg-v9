<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User\Profile\Chat;

use Closure;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class GetUserChatsQuery extends Query
{
    protected $attributes = [
        'name' => 'GetUserChatsQuery',
        'description' => 'A query to retrieve all chats of current user.'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('ChatType'));
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        return $user->chats;
    }
}
