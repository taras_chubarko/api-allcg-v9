<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User\Profile\Chat;

use App\Events\User\Profile\Chat\ChatMessagesReadEvent;
use App\Models\User\Profile\Chat\Chat;
use App\Models\User\Profile\Chat\ChatMessage;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class GetChatMessagesQuery extends Query
{
    protected $attributes = [
        'name' => 'GetChatMessagesQuery',
        'description' => 'A query to get chat messages.'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('ChatMessageType'));
    }

    public function args(): array
    {
        return [
            'chat_id' => [
                'type' => Type::string(),
            ],
            'limit' => [
                'type' => Type::int(),
                'defaultValue' => 10000,
            ],
            'offset' => [
                'type' => Type::int(),
                'defaultValue' => 0,
            ],
        ];
    }

    public function rules(array $args = []): array
    {
        return [
            'chat_id' => [
                'required',
                'exists:chats,_id',
                function ($attribute, $value, $fail) {
                    $user = auth()->user();
                    $chat = Chat::find($value);

                    if (!$chat->members->find($user->id)) {
                        $fail("You don't have an access to this chat.");
                    }
                }
            ],
            'limit' => ['min: 1'],
            'offset' => ['min:0'],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        ['chat_id' => $chatId, 'limit' => $limit, 'offset' => $offset] = $args;
        $chatMessages = ChatMessage::where('chat_id', $chatId)->latest()->get();

        if ($chatMessages->count() === 0) {
            return [];
        }

        $unreadChatMessages = $chatMessages
            ->where('sender_id', '!==', $user->id)
            ->where('status', 'sent');

        if (count($unreadChatMessages)) {
            broadcast(new ChatMessagesReadEvent($chatId))->toOthers();
            $unreadChatMessages->each(function ($message) {
                $message->markAsRead();
            });
        }

        if ($offset === 0) {
            $unreadChatMessages = $chatMessages->filter(function ($chatMessage) {
                return count($chatMessage->read) === 0;
            });

            return $unreadChatMessages
                ->merge(
                    $chatMessages
                        ->skip($unreadChatMessages->count())
                        ->take($limit)
                );
        }

        return $chatMessages->skip($offset)->take($limit);
    }
}
