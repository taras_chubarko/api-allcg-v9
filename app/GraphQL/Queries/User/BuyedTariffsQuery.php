<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\User\BuyedTariffsType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class BuyedTariffsQuery extends Query
{
    const NAME = 'BuyedTariffsQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Куплені тарифні плани користувача'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(BuyedTariffsType::NAME));
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return auth()->user()->buyedTariffs()->where('status', 1)->get();
    }
}
