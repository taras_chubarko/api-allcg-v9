<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\User\UserNotificationType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UpdateUserNotificationQuery extends Query
{
    const NAME = 'UpdateUserNotificationQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Редагування нотифікацій'
    ];

    public function type(): Type
    {
        return GraphQL::type(UserNotificationType::NAME);
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        return $user->notifySettings ? $user->notifySettings->data : null;
    }
}
