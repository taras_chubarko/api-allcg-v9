<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\User\UserEditType;
use App\GraphQL\Types\User\UserType;
use App\Models\User;
use App\Rules\IsCurrentUserRule;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserEditQuery extends Query
{
    const NAME = 'UserEditQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Данні користувача для редагування'
    ];

    public function type(): Type
    {
        return GraphQL::type(UserEditType::NAME);
    }

    public function args(): array
    {
        return [
            'id' => ['type' => Type::nonNull(Type::string())]
        ];
    }

    protected function rules(array $args = []): array
    {
        return [
            'id' => [new IsCurrentUserRule()],
        ];
    }


    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return User::query()->find($args['id']);
    }
}
