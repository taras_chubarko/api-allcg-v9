<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\Geo\LocationType;
use App\Models\GeoEN;
use App\Models\GeoRU;
use App\Models\GeoUK;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserSearchLocationQuery extends Query
{
    const NAME = 'UserSearchLocationQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(LocationType::NAME));
    }

    public function args(): array
    {
        return [
            'query' => ['type' => Type::string()],
            'lang' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $q['en'] = GeoEN::query();
        $q['uk'] = GeoUK::query();
        $q['ru'] = GeoRU::query();
        $data = $q[$args['lang']]->where('city', 'like', '%' . $args['query'] . '%')->take(10)->get();
        return $data;
    }
}
