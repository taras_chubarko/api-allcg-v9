<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\GraphQL\Types\User\UserProductsFolderType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class GetUserProductsFoldersQuery extends Query
{
    const NAME = 'GetUserProductsFoldersQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'A query'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(UserProductsFolderType::NAME));
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $user = auth()->user();
        return $user->productsFolders()->get();
    }
}
