<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\User;

use App\Facades\UserFacade;
use App\GraphQL\Types\Product\ProductType;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UserBuyedProductsQuery extends Query
{
    const NAME = 'UserBuyedProductsQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Куплені твари користувачем'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(ProductType::NAME);
    }

    public function args(): array
    {
        return [
            'per_page' => ['type' => Type::nonNull(Type::int())],
            'current_page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return UserFacade::buyedProducts(auth()->user(), $args);
    }
}
