<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Comments;

use App\GraphQL\Types\Comments\CommentType;
use App\Models\Comment;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class CommentsPaginateQuery extends Query
{
    const NAME = 'CommentsPaginateQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Сторінка товару коментарі'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(CommentType::NAME);
    }

    public function args(): array
    {
        return [
            'entity' => ['type' => Type::nonNull(Type::string())],
            'entity_id' => ['type' => Type::nonNull(Type::string())],
            'limit' => ['type' => Type::nonNull(Type::int())],
            'page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return Comment::query()
            ->with(['user', 'likes'])
            ->where('entity', $args['entity'])
            ->where('entity_id', $args['entity_id'])
            ->orderBy('created_at', 'desc')
            ->paginate($args['limit'], ['*'], 'page', $args['page']);
    }
}
