<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Catalog;

use App\GraphQL\Types\Catalog\CatalogItemType;
use App\Models\Catalog;
use App\Models\CatalogItem;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class CatalogItemsQuery extends Query
{
    const NAME = 'CatalogItemsQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Get all catalog items.'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type(CatalogItemType::NAME));
    }

    public function args(): array
    {
        return [
            'slug' => ['type' => Type::nonNull(Type::string())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $catalog = Catalog::where('slug', $args['slug'])->first();
        return CatalogItem::where('catalog_id', $catalog->id)
            ->where('parent_id', null)
            ->where('status', true)
            ->orderBy('sort', 'ASC')
            ->with('children')
            ->get();
    }
}
