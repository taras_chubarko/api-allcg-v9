<?php

declare(strict_types=1);

namespace App\GraphQL\Queries\Reviews;

use App\GraphQL\Types\Reviews\ReviewType;
use App\Models\Review;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ReviewsPaginateQuery extends Query
{
    const NAME = 'ReviewsPaginateQuery';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'Сторінка товару відгуки'
    ];

    public function type(): Type
    {
        return GraphQL::paginate(ReviewType::NAME);
    }

    public function args(): array
    {
        return [
            'entity' => ['type' => Type::nonNull(Type::string())],
            'entity_id' => ['type' => Type::nonNull(Type::string())],
            'limit' => ['type' => Type::nonNull(Type::int()),],
            'page' => ['type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, array $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        return Review::query()
            ->with(['user', 'likes'])
            ->where('entity', $args['entity'])
            ->where('entity_id', $args['entity_id'])
            ->orderBy('created_at', 'desc')
            ->paginate($args['limit'], ['*'], 'page', $args['page']);
    }
}
