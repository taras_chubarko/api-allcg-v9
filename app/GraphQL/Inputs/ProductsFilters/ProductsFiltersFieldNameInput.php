<?php

declare(strict_types=1);

namespace App\GraphQL\Inputs\ProductsFilters;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\InputType;

class ProductsFiltersFieldNameInput extends InputType
{
    const NAME = 'ProductsFiltersFieldNameInput';

    protected $attributes = [
        'name' => self::NAME,
        'description' => 'An input for products filters field name.',
    ];

    public function fields(): array
    {
        return [
            'en' => [
                'type' => Type::string(),
            ],
            'ru' => [
                'type' => Type::string(),
            ],
            'uk' => [
                'type' => Type::string(),
            ],
        ];
    }

    protected function rules(): array
    {
        return [
            'en' => ['required_without_all:ru,uk'],
            'ru' => ['required_without_all:en,uk'],
            'uk' => ['required_without_all:en,ru'],
        ];
    }
}
