<?php

namespace App\GraphQL\Schemas;

use App\GraphQL\Enums\ProductsFilters\ProductsFiltersFieldTypeEnum;
use App\GraphQL\Inputs\ProductsFilters\ProductsFiltersFieldNameInput;
use App\GraphQL\Mutations\ProductsFilters\Component\CreateProductsFiltersComponentMutation;
use App\GraphQL\Mutations\ProductsFilters\Component\DeleteProductsFiltersComponentMutation;
use App\GraphQL\Mutations\ProductsFilters\Component\UpdateProductsFiltersComponentMutation;
use App\GraphQL\Mutations\ProductsFilters\Field\CreateProductsFiltersFieldMutation;
use App\GraphQL\Mutations\ProductsFilters\Field\DeleteProductsFiltersFieldMutation;
use App\GraphQL\Mutations\ProductsFilters\Field\UpdateProductsFiltersFieldMutation;
use App\GraphQL\Queries\Catalog\CatalogItemsQuery;
use App\GraphQL\Queries\ProductsFilters\Component\CreateProductFiltersComponentAvailableCategoriesQuery;
use App\GraphQL\Queries\ProductsFilters\Component\UpdateProductFiltersComponentAvailableCategoriesQuery;
use App\GraphQL\Queries\ProductsFilters\ProductsFiltersQuery;
use App\GraphQL\Types\Catalog\CatalogItemNameType;
use App\GraphQL\Types\Catalog\CatalogItemType;
use App\GraphQL\Types\ProductsFilters\CreateProductFiltersComponentAvailableCategoriesType;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersComponentType;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersFieldNameType;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersFieldType;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersType;
use App\Http\Middleware\AuthMiddleware;
use Rebing\GraphQL\Support\Contracts\ConfigConvertible;

class AdminSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return [
            'query' => [
                // Products Filters
                UpdateProductFiltersComponentAvailableCategoriesQuery::class,
                CreateProductFiltersComponentAvailableCategoriesQuery::class,
                ProductsFiltersQuery::class,

                // Catalog Items
                CatalogItemsQuery::class,
            ],

            'mutation' => [
                // Products Filters
                CreateProductsFiltersComponentMutation::class,
                DeleteProductsFiltersComponentMutation::class,
                UpdateProductsFiltersComponentMutation::class,

                CreateProductsFiltersFieldMutation::class,
                UpdateProductsFiltersFieldMutation::class,
                DeleteProductsFiltersFieldMutation::class,
            ],

            'types' => [
                // Products Filters
                ProductsFiltersComponentType::class,
                ProductsFiltersFieldType::class,
                ProductsFiltersFieldTypeEnum::class,
                ProductsFiltersFieldNameInput::class,
                ProductsFiltersFieldNameType::class,
                ProductsFiltersType::class,
                CreateProductFiltersComponentAvailableCategoriesType::class,

                // Catalog Items
                CatalogItemType::class,
                CatalogItemNameType::class,
            ],
            // Laravel HTTP middleware
            'middleware' => [AuthMiddleware::class, 'isAdmin'],

            // Which HTTP methods to support; must be given in UPPERCASE!
            'method' => ['GET', 'POST'],

            // An array of middlewares, overrides the global ones
            'execution_middleware' => null,
        ];
    }
}
