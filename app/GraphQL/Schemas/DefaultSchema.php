<?php

namespace App\GraphQL\Schemas;

use App\GraphQL\Enums\ProductsFilters\ProductsFiltersFieldTypeEnum;
use App\GraphQL\Inputs\ProductsFilters\ProductsFiltersFieldNameInput;
use App\GraphQL\Mutations\Cart\AddToCartMutation;
use App\GraphQL\Mutations\Order\CompleteOrderMutation;
use App\GraphQL\Mutations\Order\CreateOrderMutation;
use App\GraphQL\Mutations\User\Admin\AdminAuthMutation;
use App\GraphQL\Mutations\User\CheckReCaptchaMutation;
use App\GraphQL\Mutations\User\SignInMutation;
use App\GraphQL\Mutations\User\SignUpMutation;
use App\GraphQL\Mutations\User\SocialLoginMutation;
use App\GraphQL\Mutations\User\SocialRegisterMutation;
use App\GraphQL\Queries\Cart\CartQuery;
use App\GraphQL\Queries\Catalog\CatalogItemsQuery;
use App\GraphQL\Queries\Comments\CommentsPaginateQuery;
use App\GraphQL\Queries\Product\LastProductByCategoryIdQuery;
use App\GraphQL\Queries\Product\LastProductsQuery;
use App\GraphQL\Queries\Product\ProductAuthorQuery;
use App\GraphQL\Queries\Product\ProductComponentQuery;
use App\GraphQL\Queries\Product\ProductDownloadQuery;
use App\GraphQL\Queries\Product\ProductSimilarQuery;
use App\GraphQL\Queries\Product\SearchProductsListQuery;
use App\GraphQL\Queries\ProductsFilters\Component\CategoryProductsFiltersComponentQuery;
use App\GraphQL\Queries\ProductsFilters\Component\ProductsFiltersComponentsQuery;
use App\GraphQL\Queries\ProductsFilters\Field\ProductsFiltersFieldsQuery;
use App\GraphQL\Queries\Reviews\ReviewsPaginateQuery;
use App\GraphQL\Queries\User\TopUsersQuery;
use App\GraphQL\Queries\User\UserSearchLocationQuery;
use App\GraphQL\Types\Cart\CartItemOptionsType;
use App\GraphQL\Types\Cart\CartItemsType;
use App\GraphQL\Types\Cart\CartType;
use App\GraphQL\Types\Catalog\CatalogItemNameType;
use App\GraphQL\Types\Catalog\CatalogItemType;
use App\GraphQL\Types\Catalog\CatalogType;
use App\GraphQL\Types\Comments\CommentType;
use App\GraphQL\Types\Geo\LocationType;
use App\GraphQL\Types\Other\LabelValueIntType;
use App\GraphQL\Types\Other\LabelValueType;
use App\GraphQL\Types\Product\ProductFieldsType;
use App\GraphQL\Types\Product\ProductFilesArchiveType;
use App\GraphQL\Types\Product\ProductFilesPreviewsType;
use App\GraphQL\Types\Product\ProductFilesType;
use App\GraphQL\Types\Product\ProductPriceType;
use App\GraphQL\Types\Product\ProductPrintingType;
use App\GraphQL\Types\Product\ProductRatingType;
use App\GraphQL\Types\Product\ProductSettingsType;
use App\GraphQL\Types\Product\ProductsFieldsNameType;
use App\GraphQL\Types\Product\ProductsListPricesCountType;
use App\GraphQL\Types\Product\ProductType;
use App\GraphQL\Types\Product\SearchProductsListType;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersComponentType;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersFieldNameType;
use App\GraphQL\Types\ProductsFilters\ProductsFiltersFieldType;
use App\GraphQL\Types\Reviews\ReviewFilesType;
use App\GraphQL\Types\Reviews\ReviewType;
use App\GraphQL\Types\User\UserRatingType;
use App\GraphQL\Types\User\UserType;
use App\Http\Middleware\AuthGuestMiddleware;
use App\Http\Middleware\UserLastActivity;
use Rebing\GraphQL\Support\Contracts\ConfigConvertible;

class DefaultSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return [
            'query' => [
                LastProductsQuery::class,
                LastProductByCategoryIdQuery::class,
                TopUsersQuery::class,
                'catalogItems' => CatalogItemsQuery::class,
                'productComponent' => ProductComponentQuery::class,
                CommentsPaginateQuery::class,
                ReviewsPaginateQuery::class,
                ProductSimilarQuery::class,
                ProductAuthorQuery::class,
                UserSearchLocationQuery::class,
                ProductDownloadQuery::class,
                // Products Search
                SearchProductsListQuery::class,
                // Products Filters
                ProductsFiltersComponentsQuery::class,
                ProductsFiltersFieldsQuery::class,
                CategoryProductsFiltersComponentQuery::class,
                //
                CartQuery::class,
            ],

            'mutation' => [
                SignUpMutation::class,
                SignInMutation::class,
                SocialLoginMutation::class,
                SocialRegisterMutation::class,
                AdminAuthMutation::class,
                CheckReCaptchaMutation::class,
                //
                CreateOrderMutation::class,
                CompleteOrderMutation::class,
                //
                AddToCartMutation::class,
            ],

            'types' => [
                ProductType::class,
                CatalogType::class,
                CatalogItemNameType::class,
                CatalogItemType::class,
                ProductRatingType::class,
                ProductPriceType::class,
                UserType::class,
                UserRatingType::class,
                ProductSettingsType::class,
                ProductFieldsType::class,
                ProductsFieldsNameType::class,
                ProductPrintingType::class,
                ProductFilesType::class,
                ProductFilesArchiveType::class,
                ProductFilesPreviewsType::class,
                CommentType::class,
                ReviewType::class,
                ReviewFilesType::class,
                LocationType::class,
                LabelValueType::class,
                LabelValueIntType::class,
                // Products Search
                SearchProductsListType::class,
                ProductsListPricesCountType::class,
                // Products Filters
                ProductsFiltersComponentType::class,
                ProductsFiltersFieldNameType::class,
                ProductsFiltersFieldType::class,
                ProductsFiltersFieldNameInput::class,
                ProductsFiltersFieldTypeEnum::class,
                //Cart
                CartType::class,
                CartItemsType::class,
                CartItemOptionsType::class,
            ],
            // Laravel HTTP middleware
            'middleware' => [AuthGuestMiddleware::class, UserLastActivity::class],

            // Which HTTP methods to support; must be given in UPPERCASE!
            'method' => ['GET', 'POST'],

            // An array of middlewares, overrides the global ones
            'execution_middleware' => null,
        ];
    }
}
