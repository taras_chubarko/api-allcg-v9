<?php

namespace App\GraphQL\Schemas;

use App\GraphQL\Inputs\User\UserNotificationInput;
use App\GraphQL\Mutations\Cart\AddToCartMutation;
use App\GraphQL\Mutations\Product\CreateTmpProductMutation;
use App\GraphQL\Mutations\Product\DeleteTmpProductMutation;
use App\GraphQL\Mutations\Product\UploadPreviewMutation;
use App\GraphQL\Mutations\User\BuyTariffFromCardMutation;
use App\GraphQL\Mutations\User\BuyTariffMutation;
use App\GraphQL\Mutations\User\CardDeleteMutation;
use App\GraphQL\Mutations\User\CardSetForPayOutMutation;
use App\GraphQL\Mutations\User\CardVerifyMutation;
use App\GraphQL\Mutations\User\CardVerifySaveMutation;
use App\GraphQL\Mutations\User\SaveUserPrivateMutation;
use App\GraphQL\Mutations\User\SaveUserShopMutation;
use App\GraphQL\Mutations\User\SmsActivateMutation;
use App\GraphQL\Mutations\User\SmsCodeActivateMutation;
use App\GraphQL\Mutations\User\UpdateUserNotificationMutation;
use App\GraphQL\Mutations\User\UploadUserDocsMutation;
use App\GraphQL\Mutations\User\UserAccessMutation;
use App\GraphQL\Mutations\User\UserAddSubscribersMutation;
use App\GraphQL\Mutations\User\UserDeleteBgMutation;
use App\GraphQL\Mutations\User\UserRandomBgMutation;
use App\GraphQL\Mutations\User\UserUpdateMutation;
use App\GraphQL\Mutations\User\UserUploadAvatarMutation;
use App\GraphQL\Mutations\User\UserUploadBgMutation;
use App\GraphQL\Queries\Cart\CartQuery;
use App\GraphQL\Queries\Product\ProductDownloadQuery;
use App\GraphQL\Queries\User\BuyedTariffsQuery;
use App\GraphQL\Queries\User\EditUserPrivateQuery;
use App\GraphQL\Queries\User\EditUserShopQuery;
use App\GraphQL\Queries\User\GetUserProductsFoldersQuery;
use App\GraphQL\Queries\User\ProductUserQuery;
use App\GraphQL\Queries\User\ProfileTariffsQuery;
use App\GraphQL\Queries\User\UpdateUserNotificationQuery;
use App\GraphQL\Queries\User\UserBuyedProductsQuery;
use App\GraphQL\Queries\User\UserCardQuery;
use App\GraphQL\Queries\User\UserEditQuery;
use App\GraphQL\Queries\User\UserQuery;
use App\GraphQL\Types\Cart\CartItemOptionsType;
use App\GraphQL\Types\Cart\CartItemsType;
use App\GraphQL\Types\Cart\CartType;
use App\GraphQL\Types\Catalog\CatalogItemNameType;
use App\GraphQL\Types\Catalog\CatalogItemType;
use App\GraphQL\Types\Catalog\CatalogType;
use App\GraphQL\Types\Other\LabelValueIntType;
use App\GraphQL\Types\Other\LabelValueType;
use App\GraphQL\Types\Product\ProductFieldsType;
use App\GraphQL\Types\Product\ProductFilesArchiveType;
use App\GraphQL\Types\Product\ProductFilesPreviewsType;
use App\GraphQL\Types\Product\ProductFilesType;
use App\GraphQL\Types\Product\ProductPriceType;
use App\GraphQL\Types\Product\ProductPrintingType;
use App\GraphQL\Types\Product\ProductRatingType;
use App\GraphQL\Types\Product\ProductSettingsType;
use App\GraphQL\Types\Product\ProductsFieldsNameType;
use App\GraphQL\Types\Product\ProductTariffPlanType;
use App\GraphQL\Types\Product\ProductTariffType;
use App\GraphQL\Types\Product\ProductTariffWorldType;
use App\GraphQL\Types\Product\ProductType;
use App\GraphQL\Types\User\BuyedTariffsPlanType;
use App\GraphQL\Types\User\BuyedTariffsType;
use App\GraphQL\Types\User\Profile\Chat\ChatMessageAttachmentType;
use App\GraphQL\Types\User\UserCardInfoType;
use App\GraphQL\Types\User\UserCardType;
use App\GraphQL\Types\User\UserEditType;
use App\GraphQL\Types\User\UserNotificationType;
use App\GraphQL\Types\User\UserPrivateType;
use App\GraphQL\Types\User\UserProductsFolderType;
use App\GraphQL\Types\User\UserRatingType;
use App\GraphQL\Types\User\UserShopType;
use App\GraphQL\Types\User\UserType;
use App\Http\Middleware\AuthMiddleware;

// Chats
use App\GraphQL\Types\User\Profile\Chat\ChatType;
use App\GraphQL\Enums\Chat\ChatTypeEnum;
use App\GraphQL\Types\User\Profile\Chat\ChatMessageType;
use App\GraphQL\Types\User\Profile\Chat\ChatUserType;
use App\GraphQL\Queries\User\Profile\Chat\GetChatMessagesQuery;
use App\GraphQL\Queries\User\Profile\Chat\GetUserChatsQuery;
use App\GraphQL\Mutations\User\Profile\Chat\CreateChatMutation;
use App\GraphQL\Mutations\User\Profile\Chat\SendChatMessageMutation;
use App\GraphQL\Mutations\User\Profile\Chat\ReadChatMessagesMutation;

use Rebing\GraphQL\Support\Contracts\ConfigConvertible;
use Rebing\GraphQL\Support\UploadType;


class SecretSchema implements ConfigConvertible
{
    public function toConfig(): array
    {
        return [
            'query' => [
                UserQuery::class,
                BuyedTariffsQuery::class,
                UserEditQuery::class,
                UpdateUserNotificationQuery::class,
                EditUserShopQuery::class,
                EditUserPrivateQuery::class,
                UserCardQuery::class,
                UserBuyedProductsQuery::class,
                ProductDownloadQuery::class,
                ProfileTariffsQuery::class,
                GetUserProductsFoldersQuery::class,
                ProductUserQuery::class,
                //
                CartQuery::class,
                // Chats
                GetUserChatsQuery::class,
                GetChatMessagesQuery::class,
            ],

            'mutation' => [
                SmsActivateMutation::class,
                SmsCodeActivateMutation::class,
                UserUpdateMutation::class,
                UserAddSubscribersMutation::class,
                UserRandomBgMutation::class,
                UserDeleteBgMutation::class,
                UserUploadBgMutation::class,
                UserUploadAvatarMutation::class,
                UpdateUserNotificationMutation::class,
                UserAccessMutation::class,
                UploadUserDocsMutation::class,
                SaveUserShopMutation::class,
                SaveUserPrivateMutation::class,
                CardVerifyMutation::class,
                CardVerifySaveMutation::class,
                CardDeleteMutation::class,
                CardSetForPayOutMutation::class,
                BuyTariffMutation::class,
                BuyTariffFromCardMutation::class,
                //
                AddToCartMutation::class,
                // Chats
                SendChatMessageMutation::class,
                CreateChatMutation::class,
                ReadChatMessagesMutation::class,
                //
                CreateTmpProductMutation::class,
                UploadPreviewMutation::class,
                DeleteTmpProductMutation::class,
            ],

            'types' => [
                UserType::class,
                UserRatingType::class,
                BuyedTariffsType::class,
                BuyedTariffsPlanType::class,
                UploadType::class,
                LabelValueType::class,
                LabelValueIntType::class,
                UserEditType::class,
                UserNotificationInput::class,
                UserNotificationType::class,
                UserShopType::class,
                UserPrivateType::class,
                UserCardInfoType::class,
                UserCardType::class,
                ProductType::class,
                ProductSettingsType::class,
                ProductFieldsType::class,
                ProductsFieldsNameType::class,
                ProductPrintingType::class,
                ProductFilesType::class,
                ProductFilesArchiveType::class,
                ProductFilesPreviewsType::class,
                ProductPriceType::class,
                ProductRatingType::class,
                CatalogType::class,
                CatalogItemNameType::class,
                CatalogItemType::class,
                ProductTariffPlanType::class,
                ProductTariffType::class,
                ProductTariffWorldType::class,
                UserProductsFolderType::class,
                //Cart
                CartType::class,
                CartItemsType::class,
                CartItemOptionsType::class,
                // Chats
                ChatType::class,
                ChatTypeEnum::class,
                ChatUserType::class,
                ChatMessageType::class,
                ChatMessageAttachmentType::class,
            ],
            // Laravel HTTP middleware
            'middleware' => [AuthMiddleware::class],

            // Which HTTP methods to support; must be given in UPPERCASE!
            'method' => ['GET', 'POST'],

            // An array of middlewares, overrides the global ones
            'execution_middleware' => null,
        ];
    }
}
