<?php

return [
    'recaptcha_key' =>  env('RECAPTCHA_KEY', null),
    'recaptcha_secret' =>  env('RECAPTCHA_SECRET', null),
];
