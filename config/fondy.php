<?php

return[
    'merchant_id' => env('FONDY_MERCHANT_ID', null),
    'secret_key' => env('FONDY_SECRET_KEY', null),
];
