@extends('mail.layout')

@section('style')
    <style>
        .tth{
            background: #1a202c;
            color: white;
        }
        .tth th{
            text-align: center;
            padding: 4px 10px;
        }
        .ttr td{
            padding: 4px 10px;
            border: 1px solid #a0aec0;
        }
        .bbt{
            background: #3d4852;
            width: 100px;
            color: white !important;
            border-radius: 4px;
            padding: 5px 10px;
            text-decoration: none;
            text-align: center;
            display: block;
        }
    </style>
@endsection

@section('content')
    <h1 style="width: 100%; text-align: center">Замовлення № {{config('app.order_prefix') . $order->number}}</h1>
    <table width="100%" style="border-collapse: collapse; margin-bottom: 30px;">
        <thead>
        <tr class="tth">
            <th width="20">#</th>
            <th>Назва товару</th>
            <th>Ціна</th>
            <th width="100"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($order->products as $k => $product)
            <tr class="ttr">
                <td>{{$k=1}}</td>
                <td>{{$product->name}}</td>
                <td>{{$product->price}}</td>
                <td align="center">
                    <a href="{{config('app.front_url')}}/product/download/{{$product->product_id}}"
                       class="bbt" target="_blank">Скачати</a>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr class="ttr">
            <td colspan="3">Всього до сплати:</td>
            <td align="center" style="font-weight: bold">${{$order->total}}</td>
        </tr>
        </tfoot>
    </table>
    <small style="padding-top: 50px; color: #74787e; display: block">
        Дані посилання на скачування товару діють тільки з вашої ір адреси, тому якщо у вас вона динамічно змінюється
        радимо закачати товари негайно. Або зареєструватися на сайті щоб мати постійний доступ до скачування. Дякуємо!
    </small>
@endsection
